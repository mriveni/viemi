/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.common.entity.runtime;

import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import java.util.List;

/**
 *
 * @author Jun
 */
public class ExecutionSession {
    
    MonitoringSession monitoringSession;
    List<ExecutionStep> listOfExecutionSteps;
    SLAChangeProcess adjustmentProcess;

    public ExecutionSession(MonitoringSession monitoringSession, List<ExecutionStep> listOfExecutionSteps, SLAChangeProcess adjustmentProcess) {
        this.monitoringSession = monitoringSession;
        this.listOfExecutionSteps = listOfExecutionSteps;
        this.adjustmentProcess = adjustmentProcess;
    }

    
    public MonitoringSession getMonitoringSession() {
        return monitoringSession;
    }

    public void setMonitoringSession(MonitoringSession monitoringSession) {
        this.monitoringSession = monitoringSession;
    }

    public List<ExecutionStep> getListOfExecutionSteps() {
        return listOfExecutionSteps;
    }

    public void setListOfExecutionSteps(List<ExecutionStep> listOfExecutionSteps) {
        this.listOfExecutionSteps = listOfExecutionSteps;
    }

    public SLAChangeProcess getAdjustmentProcess() {
        return adjustmentProcess;
    }

    public void setAdjustmentProcess(SLAChangeProcess adjustmentProcess) {
        this.adjustmentProcess = adjustmentProcess;
    }
    
    
    
    
    
}
