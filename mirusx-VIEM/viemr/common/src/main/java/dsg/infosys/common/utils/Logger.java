/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.common.utils;

import dsg.infosys.common.entity.eda.elasticprocess.ElasticState;
import dsg.infosys.common.entity.primitiveaction.MetricCondition;
import dsg.infosys.common.entity.primitiveaction.AdjustmentAction;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.primitiveaction.MonitoringAction;
import dsg.infosys.common.entity.eda.elasticprocess.MonitoringProcess;
import dsg.infosys.common.entity.eda.elasticprocess.ParallelGateway;
import dsg.infosys.common.entity.primitiveaction.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Jun
 */
public class Logger {
    
    public static void logInfo(String log) {
        java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.INFO, log);
    }
       
}
