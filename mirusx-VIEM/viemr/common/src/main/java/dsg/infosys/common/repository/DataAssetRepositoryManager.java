/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.common.repository;

import dsg.infosys.common.entity.dataanalyticsfunction.DataAnalyticsFunction;

import dsg.infosys.common.utils.Configuration;
import dsg.infosys.common.utils.RestfulWSClient;
import dsg.infosys.common.utils.Logger;
import dsg.infosys.common.utils.MySqlConnectionManager;
import dsg.infosys.common.utils.YamlUtils;


/**
 *
 * @author Jun
 */
public class DataAssetRepositoryManager {
    
  
    String classPath;

    public DataAssetRepositoryManager(String classPath) {
        this.classPath = classPath;
       
    }
    
    
    
    
    public String requestToGetDataAsset(DataAnalyticsFunction daf){
        
        Configuration configuration  = new Configuration(classPath);
        String ip = configuration.getConfig("DATA.ASSET.LOADER.IP");
        String port = configuration.getConfig("DATA.ASSET.LOADER.PORT");
        String resource = configuration.getConfig("DATA.ASSET.LOADER.RESOURCE.REQUEST");
      
        
        Logger.logInfo("IP: " + ip);
        Logger.logInfo("PORT: " + port);
        Logger.logInfo("RESOURSE: " + resource);
        
        String dafYaml = YamlUtils.marshallYaml(DataAnalyticsFunction.class, daf);
        
        RestfulWSClient ws = new RestfulWSClient(ip, port, resource);
        String noOfDataPartitions = ws.callPutMethod(dafYaml);
   
        return noOfDataPartitions;
    }
    
    
    
    
}
