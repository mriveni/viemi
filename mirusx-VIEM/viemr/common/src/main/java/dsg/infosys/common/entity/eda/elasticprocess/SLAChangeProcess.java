/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.common.entity.eda.elasticprocess;

import dsg.infosys.common.entity.primitiveaction.AdjustmentAction;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jun
 */

@XmlRootElement(name = "SLAChangeProcess")
@XmlAccessorType(XmlAccessType.FIELD)
public class SLAChangeProcess {


    
    @XmlElement(name = "listOfTasks", required = true)
    List<Task> listOfTasks;
    
    @XmlElement(name = "directedAcyclicalGraph", required = true)
    DirectedAcyclicalGraph directedAcyclicalGraph;
    
    

    public SLAChangeProcess() {
    }

    public SLAChangeProcess(List<Task> listOfTasks, DirectedAcyclicalGraph directedAcyclicalGraph) {
        this.listOfTasks = listOfTasks;
        this.directedAcyclicalGraph = directedAcyclicalGraph;
    }

    public List<Task> getListOfTasks() {
        return listOfTasks;
    }

    public void setListOfTasks(List<Task> listOfTasks) {
        this.listOfTasks = listOfTasks;
    }

    public DirectedAcyclicalGraph getDirectedAcyclicalGraph() {
        return directedAcyclicalGraph;
    }

    public void setDirectedAcyclicalGraph(DirectedAcyclicalGraph directedAcyclicalGraph) {
        this.directedAcyclicalGraph = directedAcyclicalGraph;
    }

    

    
    
}
