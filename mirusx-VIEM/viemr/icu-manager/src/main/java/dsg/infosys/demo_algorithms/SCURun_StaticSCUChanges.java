/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.demo_algorithms;

import dsg.infosys.basemodel.ICU;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.basemodel.ICUPropList;
import dsg.infosys.basemodel.ICUTask;
import dsg.infosys.basemodel.Metrics;
import dsg.infosys.basemodel.MetricsList;
import dsg.infosys.basemodel.MetricsSCU;
import dsg.infosys.basemodel.SCU;
import dsg.infosys.basemodel.TaskPool;
import dsg.infosys.basemodel.TaskQueue;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import javax.xml.bind.JAXBException;
import dsg.infosys.ranking.AHPQoSRanking;

import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;



/**
 *
 * @author Mirela Riveni
 */
public class SCURun_StaticSCUChanges {  
private static ICU icu, icutemp, currentICU, icuscu, changeicu;    
private static ICUTask task, tempTask, currentTask;
public static ICUList icus = new ICUList();   
public static ICUList icus_ = new ICUList();   
public static ICUList tempicus;
public static ICUList iculistcurrent;
public static TaskQueue taskList, allicutasks, dynamicicutasklist, currentICUtaskList;
public static TaskPool taskPool;
static final AHPQoSRanking selectedicus=new AHPQoSRanking();
private static SCU currentSCU;
public static ICUList icuPool, icusnew, icusloop;
public static int[] minutes = new int[300]; 
private static int queueTime, tempTime, totalTime=0, invocationTime, totalTaskTime;
private static ICU tempICU;
private static MetricsSCU metricsscu;
private static ArrayList<String> skillCostreq;
public ICUList icuinvocationlist, icuinvocations, currentinvocations, icutempinvocations, invocations;
public static HashMap<String, Double> skillcost;
public  static MetricsList metricsList;
public static ICUPropList prop = new ICUPropList();
public static int propTemp;
public static double randCost;
public static String skilltemp;
public static double costchange; 
public static int min;
public static int totaltasks=0, totaldelt=0, totalnondelt, sucdelt, sucnondelt, totaltemp, totaldelinvoc=0;
double sucr, delsucr, nondelsucr,  willconf, reliab, prod, pt, mcts, socioTechnicalTrust;
boolean will;
DecimalFormat df = new DecimalFormat("#.0000"); 


public static TaskPool generateTasks(){
    
    taskPool=new TaskPool();
    taskPool.createHardcodedTaskPool();
    
    return taskPool;
}

public SCURun_StaticSCUChanges() {
    this.metricsList = new MetricsList();
}
    
    
public ICUList generateICUs() throws JAXBException, IOException, FileNotFoundException, URISyntaxException{
    icus_=selectedicus.getRankedICUs();
    icus_.setICUs(new ICUList());
    icus_.setICUs(selectedicus.getRankedICUs());
    return icus_;
} 

public void clearICUTrustScores(ICUList icus){
    
    invocationTime=0; sucr=0.0; delsucr=0.0; nondelsucr=0.0;  willconf=0.0; reliab=0.0; prod=0.0; pt=0.0; mcts=0.0; socioTechnicalTrust=0.0; totaldelt=0; totaltemp=0; sucnondelt=0;
    for (int j=0; j<icus.size();j++){
        changeicu=new ICU();
        changeicu=icus.get(j);
        changeicu.ICUMetrics_.setTotalAssignedTasks(totaltemp);
        changeicu.ICUMetrics_.setSuccDelTasks(sucdelt);
        changeicu.ICUMetrics_.setTotalDelTasks(totaldelt);
        changeicu.ICUMetrics_.setSuccessRate(sucr);
        changeicu.ICUMetrics_.setSuccNonDelTasks(sucnondelt);
        //changeicu.ICUMetrics_.setICUInvocations(icutempinvocations);
        changeicu.ICUMetrics_.setDelegatedSuccessRate(delsucr);
        changeicu.ICUMetrics_.setNonDelSuccessRate(nondelsucr);
        changeicu.ICUMetrics_.setICUReliability(reliab);
        changeicu.ICUMetrics_.setWillingnessConf(willconf);
        changeicu.ICUMetrics_.setICUPerformanceTrustScore(pt);
        changeicu.ICUMetrics_.setICUProductivity(prod);
        changeicu.ICUMetrics_.setSocioTechnicalTrust(socioTechnicalTrust);
        
    }
} 


  public ICUList algo(ICUList icus, boolean changeSLA) throws IOException{
  
    System.out.println("ASSIGNING A NEW BAG OF TASKS TO THE SCU...");
    currentSCU = new SCU();
    //allicus=selectedicus.getRankedICUs();
    taskList=generateTasks().getTaskPool();
    //taskList=taskPool.getTaskPool();
    //allicus = icuPool.getICUPool();
    
    task = new ICUTask();
    tempTask = new ICUTask();
    icu = new ICU();
    icutemp = new ICU();
    currentICU = new ICU();
    currentICUtaskList=currentICU.getICUTaskQueue();
 
    //icus=icuPool.getICUPool();
    for (int i=0; i<taskList.size(); i++){
        queueTime=(int)(Math.random()* (240 - 20) + 20);
        task=taskList.get(i);
       // totalTime=totalTime+task.getTaskTime();
        //hardcoded taskQueueTime
        if (queueTime<task.getTaskDeadline()){
            task.setTaskInQueue(queueTime);
        }
        else {
            task.setTaskInQueue(task.getTaskDeadline()-5);
        }
        
        //assign tasks to ICUs with matching skills and form the SCU
        for (int j=0; j<icus.size();j++){
            totaltasks=0;
           
            icu=icus.get(j);
            if(task.getSkillReq().equals(icu.getICUSkill())){
                icu.totaltasks_++;
                invocationTime=invocationTime+task.getTaskTime();
                will=icu.getWillingness();
            
                icu.getICUMetrics().updateICUTaskQueue(task);
                //icu.ICUMetrics_.calICUInvocations(icu); 
                icu.ICUMetrics_.setTotalAssignedTasks(icu.getICUTaskQueue().size());
                task.setTaskOwner(icu);
                if((!currentSCU.contains(icu))){
                    currentSCU.addICU(icu, currentSCU); //add(icu);
            }
            task.setTaskState(6);
            System.out.println("Task with id "+task.getTaskId()+"is assigned to ICU with id"+icu.getICUId());
            //System.out.println("task queue time "+task.getTaskInQueue()+" deadline "+task.getTaskDeadline());
            break;
           }
        }//continue;
           //else 
           //System.out.println("No appropriate ICU found for this task.");
           //System.out.print(icu.getICUId());
           //System.out.print(task.getTaskId());
    }
    
    
 
   if(changeSLA == false ){ 
        TimeThresholdDelegation(icus, false);
   }
   else{
       TimeThresholdDelegation(icus, true);
   }
   getTotalTime();
   currentSCU.setSCUMembers(currentSCU);
   
    for (int k=0; k<icus.size();k++){
    tempICU=icus.get(k);
   
    tempICU.ICUMetrics_.setMICUid(tempICU.getICUId());
    tempICU.ICUMetrics_.calICUCollaborationScore();
    mcts=tempICU.ICUMetrics_.getMCTS();

    tempICU.ICUMetrics_.setMCTS(mcts);
      
    
    tempICU.ICUMetrics_.calNonDelTasks();
    tempICU.ICUMetrics_.calSuccessfulDelNonDelTasks(tempICU.ICUMetrics_.getAllICUTasks());
    
    sucdelt=tempICU.ICUMetrics_.getSuccDelTasks();
    sucnondelt=tempICU.ICUMetrics_.getSuccNonDelTasks();
    totaldelt=tempICU.ICUMetrics_.getTotalDelTasks();
    totaltemp= tempICU.ICUMetrics_.getAllICUTasks().size();


    totalnondelt=totaltemp-totaldelt;
   

    icutemp.ICUMetrics_.setTotalNonDelTasks(totalnondelt);
    icutemp.ICUMetrics_.setSuccNonDelTasks(sucnondelt);

    
    tempICU.ICUMetrics_.calDelNonDelSuccessRate(tempICU.ICUMetrics_.getAllICUTasks());
    
    
    delsucr=tempICU.ICUMetrics_.getDelegatedSuccessRate();
    nondelsucr=tempICU.ICUMetrics_.getNonDelSuccessRate();
    
    
    tempICU.ICUMetrics_.calSuccessRate(icus.get(k).ICUMetrics_.getAllICUTasks());
    sucr=tempICU.ICUMetrics_.getICUSuccessRate();
    
    
    //icutempinvocations=tempICU.ICUMetrics_.getICUInvocations();
    
    tempICU.ICUMetrics_.calICUreliability();
    reliab=tempICU.ICUMetrics_.getICUreliability();
    
    tempICU.ICUMetrics_.calICUProductivity(icus.get(k).ICUMetrics_.getAllICUTasks());
    prod=tempICU.ICUMetrics_.getICUProductivity();
    
    
    tempICU.ICUMetrics_.calICUwillingnessConf(icus.get(k));
    willconf=tempICU.ICUMetrics_.getICUWillingnessConf();
    
    
    
    tempICU.ICUMetrics_.calICUPerformanceTrustScore();
    pt=tempICU.ICUMetrics_.getICUPerformanceTrustScore();
    
    
    tempICU.ICUMetrics_.calICUsocioTechnicalTrust();    
    socioTechnicalTrust=tempICU.ICUMetrics_.getICUSocioTechnicalTrust();
    
    tempICU.ICUMetrics_.setTotalAssignedTasks(totaltemp);
    tempICU.ICUMetrics_.setSuccDelTasks(sucdelt);
    tempICU.ICUMetrics_.setTotalDelTasks(totaldelt);
    tempICU.ICUMetrics_.setSuccessRate(sucr);
    //tempICU.ICUMetrics_.setSuccNonDelTasks(sucnondelt);
    //tempICU.ICUMetrics_.setICUInvocations(icutempinvocations);
    tempICU.ICUMetrics_.setDelegatedSuccessRate(delsucr);
    tempICU.ICUMetrics_.setNonDelSuccessRate(nondelsucr);
    tempICU.ICUMetrics_.setICUReliability(reliab);
    tempICU.ICUMetrics_.setWillingnessConf(willconf);
    tempICU.ICUMetrics_.setICUPerformanceTrustScore(pt);
    tempICU.ICUMetrics_.setICUProductivity(prod);
    tempICU.ICUMetrics_.setSocioTechnicalTrust(socioTechnicalTrust);
    
    
    tempicus.add(tempICU);

    metricsList.add(tempICU.ICUMetrics_);
    setMetricList(metricsList);
       // System.out.println(icus.get(k).getICUId()+indent+indent+icus.get(k).ICUMetrics_.getAllICUTasks().size()+indent+indent+String.format("%.4f",tempICU.ICUMetrics_.getMCTS())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUPerformanceTrustScore())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUSocioTechnicalTrust())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUProductivity())+indent+indent+icus.get(k).ICUMetrics_.getSuccessfulICUTaskQueue().size());    
    }
      
    return tempicus;
}
public static void TimeThresholdDelegation(ICUList icus, boolean changeSLA) throws IOException{

    int a; 
    //simple FCFS delegations
    if (changeSLA==false){
    for (min=0; min<300; min++){ 
        for(int j=0; j<taskList.size(); j++){
            tempTask=taskList.get(j);
       
            tempTime=tempTask.getTaskInQueue();
            if(tempTime==min && (tempTime == tempTask.getTaskDeadline()/2)){
                //if(tempTime==min && (tempTime ==(currentICU.getICUTaskQueue().get(j).getTaskDeadline()/2))){
                currentICU=tempTask.getTaskOwner();
                //currentICU.ICUMetrics_.calICUInvocations(currentICU);
                System.out.println();
                System.out.println("Task waiting-time for task with id "+tempTask.getTaskId()+" reached the pre-set threshold, delegating task...");
              
                for (a=0; a<icus.size(); a++){
                
                    icutemp=icus.get(a);  
                       
                    if(tempTask.getTaskOwner().getICUId()!=icutemp.getICUId()){
                    if(tempTask.getSkillReq().equals(icutemp.getICUSkill())){
                          
                        icutemp.updateICUTaskQueue(tempTask);
                        icutemp.ICUMetrics_.updateICUTaskQueue(tempTask);
                        currentICU.ICUMetrics_.removeTask(tempTask);
                        currentICU.failedTasks_++;
                        icutemp.ICUMetrics_.updateDelegatedTasks(tempTask);
                     
                        if((!currentSCU.contains(icutemp))){
                            currentSCU.addICU(icutemp, currentSCU); //add(icu);

                        }
                             
                         System.out.println("Task "+tempTask.getTaskId()+" delegated from ICU with id "+ tempTask.getTaskOwner().getICUId() + " to ICU with id "+ icutemp.getICUId());
                        tempTask.setTaskState(9);
                        //icutemp.ICUMetrics_.
                        tempTask.setTaskOwner(icutemp);
                        icutemp.ICUMetrics_.getAllICUTasks().getTaskwithId(tempTask.getTaskId()).taskDelegated=true;   
                               
                        break;
                    }
                    }
                    //break
                } 
            }
        }
    }//currentSCUMembers();
    }//end if time threshold
    else {
        setICUCostperSkillReq();
        setParameterChangesReq();
        for(int k=0; k<taskList.size(); k++){
            tempTask=taskList.get(k);
            currentICU=tempTask.getTaskOwner();
            //currentICU=tempTask.getTaskOwner();
            outerloop:
            for (int b=0; b<icus.size(); b++){
                icutemp=icus.get(b); 
                if(tempTask.getTaskOwner().getICUId()!=icutemp.getICUId()){
             {
                    innerloop:
                    for (Map.Entry<String, Double> entry : skillcost.entrySet()) {
                    if((tempTask.getSkillReq().equals(icutemp.getICUSkill()))&&(entry.getKey().equals(icutemp.getICUSkill()))&&(entry.getValue()>icutemp.getICUCostPerTask())&&(entry.getValue()<currentICU.getICUCostPerTask())&&(entry.getValue()!=0)){
                        
                    icutemp.updateICUTaskQueue(tempTask);

                    icutemp.ICUMetrics_.updateICUTaskQueue(tempTask);
               
                    currentICU.ICUMetrics_.removeTask(tempTask);
                    currentICU.failedTasks_++;
                    icutemp.ICUMetrics_.updateDelegatedTasks(tempTask);
                    currentSCU.addICU(icutemp, currentSCU); //add(icu);
                    invocationTime=invocationTime+tempTask.getTaskTime();
                    System.out.println();
                    System.out.print("Task "+tempTask.getTaskId()+" delegated from ICU with id "+ tempTask.getTaskOwner().getICUId() + "and cost "+ tempTask.getTaskOwner().getICUCostPerTask() +" and skill "+ tempTask.getTaskOwner().getICUSkill());
                    tempTask.setTaskState(9);
      
                    tempTask.setTaskOwner(icutemp);
                    System.out.println(" to ICU with id "+ icutemp.getICUId()+ " and new cost " + tempTask.getTaskOwner().getICUCostPerTask()+ " " + entry.getValue()+ " skill " + icutemp.getICUSkill()+  " "+icutemp.getICUCostPerTask());
                    icutemp.ICUMetrics_.getAllICUTasks().getTaskwithId(tempTask.getTaskId()).taskDelegated=true;   
                    break outerloop;
                    } //break;
      
              }
                    
            }
            }
                
            } //end icu loop
           
        }//end task loop
    
    }//end if else
}//end method
  
public static void setICUCostperSkillReq(){
    skillCostreq = new ArrayList<>();
    skillcost = new HashMap<>();
    costchange = 0.0;
    propTemp = (int)(Math.random()*5);
    for (ICUTask t : taskList){
       if(!skillCostreq.contains(t.getSkillReq())){
        skillCostreq.add(t.getSkillReq());
       }
    }
    for (Iterator<String> it = skillCostreq.iterator(); it.hasNext();) {
        skilltemp = it.next();
        skillcost.put(skilltemp, costchange);
    }
}

public  static void setParameterChangesReq() throws IOException{
   Object value=0;
    System.out.println("Do you want to change the fee for the following skills: ");
    for (int i=0; i<skillCostreq.size(); i++){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(skillCostreq.get(i).toString());
        System.out.println("Y/N: ");
        String answer = br.readLine();
        if(answer.equalsIgnoreCase("N")){
            System.out.println("The fee for skill: "+skillCostreq.get(i).toString()+" not changed");
            break;
        }
        else if(answer.equalsIgnoreCase("Y")){
            System.out.println("Enter a (double) number if you want to change the fee for work with the skill: "+skillCostreq.get(i).toString());
            try{
                double input = Double.parseDouble(br.readLine());
                value=input;
                skillcost.put(skillCostreq.get(i).toString(), (Double) value);
                
            }
            catch(NumberFormatException invalidnr){
                System.err.println("Invalid Number Format!");
            }
          
            System.out.println("The fee for skill: "+skillCostreq.get(i).toString()+" changed to "+value);
            
        }  
        else {System.out.println("Wrong input, try again "); i--;}
    }//end for
}//end method


public static void main(String[] args) throws JAXBException, IOException, FileNotFoundException, URISyntaxException{

     
    SCURun_StaticSCUChanges sttalgo = new SCURun_StaticSCUChanges();
    tempicus = new ICUList();
   
    icusnew=new ICUList();
    icusloop=new ICUList();
    icusnew=sttalgo.generateICUs();

    for(int i=0; i<10; i++){
        System.out.println();
        sttalgo.clearICUTrustScores(icusnew);
        
        if (i==5){
        sttalgo.algo(icusnew, true);
        }
        
        else{
        sttalgo.algo(icusnew, false); 
          
        }
   
        sttalgo.currentSCUMembers();

}
}


public static void setMetricList(MetricsList metricsList){
    SCURun_StaticSCUChanges.metricsList=metricsList;
}

public MetricsList getMetricList(){
    return metricsList;
}


public int getTotalTime(){
    for (int t=0; t<taskList.size(); t++){
        currentTask=taskList.get(t);
        totalTime=totalTime+currentTask.getTaskTime();
        if (currentTask.taskDelegated=true)
        {
         totalTime=totalTime+(currentTask.getTaskTime()*2);
        }
    }
    return totalTime;
}


public  void currentSCUMembers() throws IOException{
        
   iculistcurrent=new ICUList();

   metricsscu=new MetricsSCU();

   iculistcurrent=currentSCU.getSCUMembers();
 
   String indent = "    ";
   System.out.println();
   System.out.println("========== OUTPUT: CURRENT SCU Members ==========");
   System.out.println("ICU ID" + indent + "alltasks" + indent + "mcts" + indent + "ptrust" + indent  + "stt"+indent+" productivity"+indent+"succ tasks"+indent+"success rate");
 
   for(int j=0;j<currentSCU.getSCUMembers().size();j++){
 
       
        System.out.println();
        icuscu=currentSCU.getSCUMembers().get(j);

        System.out.println(icuscu.getICUId()+indent+indent+icuscu.ICUMetrics_.getAllICUTasks().size()+indent+indent+String.format("%.4f",icuscu.ICUMetrics_.getMCTS())+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUPerformanceTrustScore())+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUSocioTechnicalTrust())+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUProductivity())+indent+indent+indent+icuscu.ICUMetrics_.getSuccessfulICUTaskQueue().size()+indent+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUSuccessRate()));          
   }
       metricsscu.calProductivity(currentSCU);  
       metricsscu.calPerformanceTrustScore(currentSCU);
       metricsscu.calSocialTrustScore(currentSCU);
       metricsscu.calSocioTechnicalTrust();
       metricsscu.calTotalSCUTasks(currentSCU);
       metricsscu.calTotalSCUDelegatedTasks(currentSCU);
       totaldelinvoc=totaldelinvoc+metricsscu.totalscudelegatedtasks_;
       System.out.println("========== SCU TRUST-BASED METRICS RESULTS ==========");
       System.out.println("SCU Id: "+currentSCU.getSCUId()+ " Productivity: " + String.format("%.4f",metricsscu.productivitySCU_) + " Performance trust: "+String.format("%.4f",metricsscu.technicalTrustSCU_ )+" Social trust: "+ String.format("%.4f",metricsscu.socialTrustSCU_ )+" Socio-technical trust: "+String.format("%.4f",metricsscu.socioTechnicalTrustSCU_));   
       System.out.println("SCU total tasks: "+metricsscu.totalscutasks_+" SCU invocation delegated tasks: "+metricsscu.totalscudelegatedtasks_+" total delegated tasks "+totaldelinvoc);
       System.out.println("Adaptation tasks time: "+invocationTime+" Total tasks time "+totalTime);

} 
    
}
