/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.demo_algorithms;

import java.io.IOException;
import java.util.List;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.runtime.ExecutionStep;
import dsg.infosys.process.engine.dataelasticitycontroller.DEPExecutionPlanning;

public class SLOChangesDemoMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here

        SCURun_SLOChanges sttalgo = new SCURun_SLOChanges();


        ICUList listOfSelectedICUs = sttalgo.generateICUs();

        for (int i = 0; i < 10; i++) {
            System.out.println();
            sttalgo.clearICUTrustScores(listOfSelectedICUs);

            if (i == 5) {
                sttalgo.algo(listOfSelectedICUs, true);
            } else {
                sttalgo.algo(listOfSelectedICUs, false);

            }
            sttalgo.currentSCUMembers();

        }

    }

}
