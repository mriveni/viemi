/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.demo_algorithms;

import dsg.infosys.basemodel.ICU;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.basemodel.ICUPropList;
import dsg.infosys.basemodel.ICUTask;
import dsg.infosys.basemodel.Metrics;
import dsg.infosys.basemodel.MetricsList;
import dsg.infosys.basemodel.MetricsSCU;
import dsg.infosys.basemodel.SCU;
import dsg.infosys.basemodel.TaskPool;
import dsg.infosys.basemodel.TaskQueue;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import javax.xml.bind.JAXBException;
import dsg.infosys.ranking.AHPQoSRanking;

import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import dsg.infosys.common.entity.eda.elasticprocess.Action;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.eda.elasticprocess.DirectedAcyclicalGraph;
import dsg.infosys.common.entity.eda.elasticprocess.ParallelGateway;

/**
 *
 * @author Mirela Riveni
 */
public class SCURun_SLOChanges {

    private static ICU icu, icutemp, currentICU, icuscu, changeicu;
    private static ICUTask task, tempTask;
    public static ICUList icus = new ICUList();
    public static ICUList icus_ = new ICUList();
    public static ICUList tempicus;
    public static ICUList iculistcurrent;
    public static TaskQueue taskList, allicutasks, dynamicicutasklist, currentICUtaskList;
    public static TaskPool taskPool;
    static final AHPQoSRanking selectedicus = new AHPQoSRanking();
    private static SCU currentSCU;
    public static ICUList icuPool, icusnew, icusloop;
    public static int[] minutes = new int[300];
    private static int queueTime, tempTime;
    private static ICU tempICU;
    private static Metrics metrics;
    private static MetricsSCU metricsscu;
    private static ArrayList<String> skillCostreq;
    public ICUList icuinvocationlist, icuinvocations, currentinvocations, icutempinvocations, invocations;
    public static HashMap<String, Double> skillcost;
    public static MetricsList metricsList;
    public static ICUPropList prop = new ICUPropList();
    public static int propTemp;
    public static double randCost;
    public static String skilltemp;
    public static double costchange;
    public static int min;
    public static int totaltasks = 0, totaldelt = 0, totalnondelt, sucdelt, sucnondelt, totaltemp, totaldelinvoc = 0, acks=0, reqs=0, invocnr=0;
    double sucr, delsucr, nondelsucr, willconf=0.0, reliab, prod, pt, mcts, socioTechnicalTrust;
    boolean will;
    private ICUTask currentTask;
    private int totalTime;
    private static SLAChangeProcess icuProcess;
    
    DecimalFormat df = new DecimalFormat("#.0000");


    //constructor
    public SCURun_SLOChanges() {
        //a metricsList for ICUs, to store the metrics in XML
        this.metricsList = new MetricsList();
        tempicus = new ICUList();
        
        //icusnew stores the pool of ranked ICUs from AHPQoSRanking.java
        icusnew = new ICUList();
        
        icusloop = new ICUList();
    }

    /*Generate ICUs with an AHP-based ranking algorithm using the 'AHPQoSRanking.java' class in the 'ranking' package.
    Returns a pool of ranked resources (ranked by three different type of metrics).*/
    public ICUList generateICUs() throws JAXBException, IOException, FileNotFoundException, URISyntaxException {
        icus_ = selectedicus.getRankedICUs();
        icus_.setICUs(new ICUList());
        icus_.setICUs(selectedicus.getRankedICUs());
        icusnew = icus_;
        return icus_;
    }
    
    //Generate Tasks for ICUs from TaskPool.java.
    public static TaskPool generateTasks() {
        taskPool = new TaskPool();
        taskPool.createHardcodedTaskPool();
        return taskPool;
    }

    public void clearICUTrustScores(ICUList icus) {
        sucr = 0.0;
        delsucr = 0.0;
        nondelsucr = 0.0;
        willconf = 0.0;
        reliab = 0.0;
        prod = 0.0;
        pt = 0.0;
        mcts = 0.0;
        socioTechnicalTrust = 0.0;
        totaldelt = 0;
        totaltemp = 0;
        sucnondelt = 0;
        //acks = 0;
        //reqs = 0;
        
        for (int j = 0; j < icus.size(); j++) {
            changeicu = new ICU();
            changeicu = icus.get(j);
            //changeicu.ICUMetrics_.setICUAcks(acks);
            //changeicu.ICUMetrics_.setICURequests(reqs);
            changeicu.ICUMetrics_.setTotalAssignedTasks(totaltemp);
            changeicu.ICUMetrics_.setSuccDelTasks(sucdelt);
            changeicu.ICUMetrics_.setTotalDelTasks(totaldelt);
            changeicu.ICUMetrics_.setSuccessRate(sucr);
            changeicu.ICUMetrics_.setSuccNonDelTasks(sucnondelt);
            //changeicu.ICUMetrics_.setICUInvocations(icutempinvocations);
            changeicu.ICUMetrics_.setDelegatedSuccessRate(delsucr);
            changeicu.ICUMetrics_.setNonDelSuccessRate(nondelsucr);
            changeicu.ICUMetrics_.setICUReliability(reliab);
            changeicu.ICUMetrics_.setWillingnessConf(willconf);
            changeicu.ICUMetrics_.setICUPerformanceTrustScore(pt);
            changeicu.ICUMetrics_.setICUProductivity(prod);
            changeicu.ICUMetrics_.setSocioTechnicalTrust(socioTechnicalTrust);
        }
    }

    // The main steps of the adaptation algorithm.
    public ICUList algo(ICUList icus, boolean changeSLA) throws IOException {

        System.out.println("ASSIGNING A NEW BAG OF TASKS TO THE SCU...");
        
        
        List<Action> listOfActions = new ArrayList<>();
        List<ParallelGateway> listOfParallelGateways = new ArrayList<>();
        Action previousAction = null;
       
       
        currentSCU = new SCU();

        //Get the tasks to be assigned to ICUs.
        taskList = generateTasks().getTaskPool();
        
        //Some temporary variables for tasks and ICUs to manipulate the delegations.
        task = new ICUTask();
        tempTask = new ICUTask();
        icu = new ICU();
        icutemp = new ICU();
        currentICU = new ICU();
        
        //currentICUtaskList stores the Tasks assigned to an ICU
        currentICUtaskList = currentICU.getICUTaskQueue();
        
        
       //A for loop going through every task from the pool of tasks, so that it can be assigned to an ICU.
        for (int i = 0; i < taskList.size(); i++) {
            queueTime = (int) (Math.random() * (240 - 20) + 20);
            task = taskList.get(i);

            //hardcoded taskQueueTime
            if (queueTime < task.getTaskDeadline()) {
                task.setTaskInQueue(queueTime);
            } else {
                task.setTaskInQueue(task.getTaskDeadline() - 5);
            }

            /*Loop through ICUs to assign the task from the outer loop to an ICU with matching skills.
            ICUs are already ranked by metrics so we now search for the first ICU with a matching skill to the skill that the task requires.*/
            for (int j = 0; j < icus.size(); j++) {
                totaltasks = 0;
                icu = icus.get(j);
                
                //Check if task-skill and ICU-skill match.
                if (task.getSkillReq().equals(icu.getICUSkill())) {
                    //Update the number of total assigned tasks for the particular ICU.
                    icu.totaltasks_++;
                    icu.ICUMetrics_.setICUInvocationnr(invocnr);
                    icu.ICUMetrics_.updateInvocationnr();
                    icu.ICUMetrics_.setTotalAssignedTasks(icu.getICUTaskQueue().size());
                    
                    will = icu.getWillingness();

                    //Assign the particual task to the ICU.
                    icu.getICUMetrics().updateICUTaskQueue(task);
                    
                    //Register the ICU, to which the task is assigned.
                    task.setTaskOwner(icu);
                    
                    /*Check if the ICU to which the new task is assigned, already belongs to the SCU.
                    If it doesn't, include it in the SCU.*/
                    if ((!currentSCU.contains(icu))){
                        currentSCU.addICU(icu, currentSCU); //add(icu);
                    }
                    task.setTaskState(6);
                    System.out.println("Task with id " + task.getTaskId() + "is assigned to ICU with id" + icu.getICUId());
                    //System.out.println("task queue time "+task.getTaskInQueue()+" deadline "+task.getTaskDeadline());
                    
                    //MAKE PROCESS HERE
                    Action action = makeActionForProcess(task);
                
                    if (previousAction!=null) {
                        previousAction.setOutgoing(action.getActionID());
                        action.setIncomming(previousAction.getActionID());
                    } 
                    previousAction = action;
                    listOfActions.add(action);
                    
                    break;
                }
            }//continue;
            //else 
            //System.out.println("No appropriate ICU found for this task.");
            //System.out.print(icu.getICUId());
            //System.out.print(task.getTaskId());
        }
        

        /*Two SCU adaptation cases: 1) tasks which are in the queue of an ICU too long and they reach a pre-defined threshold, are delegated to other ICUs with method
          timeBasedDelegations(icus); and 2) if an SLA parameter, such as cost is changed at runtime for a specific skill-type, 
        the tasks with that skill-type that are already assigned, are delegated to other ICUs with the required cost
        if the current task-owner does not accept the cost change. The latter is done in method negotiateSLA(icus);*/
        if (changeSLA == false) {
            timeBasedDelegations(icus);
        } else {
            negotiateSLAPar(icus);
        }  
        
        
        getTotalTime();
        currentSCU.setSCUMembers(currentSCU);
        
        //Update all metrics for all ICUs in the pool.
        for (int k = 0; k < icus.size(); k++) {
            tempICU = icus.get(k);
            //System.out.println("Total Nr of Tasks of ICU "+icus.get(k).getICUId()+" is: "+icus.get(k).getICUTaskQueue().size() + " successful nr of tasks is: "+icus.get(k).getSuccessfulICUTaskQueue().size());
            //System.out.println("Total Nr of Tasks of ICU "+icus.get(k).getICUId()+" is: "+icus.get(k).ICUMetrics_.getAllICUTasks().size() + " successful nr of tasks is: "+icus.get(k).ICUMetrics_.getSuccessfulICUTaskQueue().size());
            tempICU.ICUMetrics_.setMICUid(tempICU.getICUId());
            tempICU.ICUMetrics_.calICUCollaborationScore();
            mcts = tempICU.ICUMetrics_.getMCTS();

            tempICU.ICUMetrics_.setMCTS(mcts);

            tempICU.ICUMetrics_.calNonDelTasks();
            tempICU.ICUMetrics_.calSuccessfulDelNonDelTasks(tempICU.ICUMetrics_.getAllICUTasks());

            sucdelt = tempICU.ICUMetrics_.getSuccDelTasks();
            sucnondelt = tempICU.ICUMetrics_.getSuccNonDelTasks();
            totaldelt = tempICU.ICUMetrics_.getTotalDelTasks();
            totaltemp = tempICU.ICUMetrics_.getAllICUTasks().size();

            totalnondelt = totaltemp - totaldelt;

            icutemp.ICUMetrics_.setTotalNonDelTasks(totalnondelt);
            icutemp.ICUMetrics_.setSuccNonDelTasks(sucnondelt);

            tempICU.ICUMetrics_.calDelNonDelSuccessRate(tempICU.ICUMetrics_.getAllICUTasks());

            delsucr = tempICU.ICUMetrics_.getDelegatedSuccessRate();
            nondelsucr = tempICU.ICUMetrics_.getNonDelSuccessRate();

            tempICU.ICUMetrics_.calSuccessRate(icus.get(k).ICUMetrics_.getAllICUTasks());
            sucr = tempICU.ICUMetrics_.getICUSuccessRate();

            tempICU.ICUMetrics_.calICUreliability();
            reliab = tempICU.ICUMetrics_.getICUreliability();

            tempICU.ICUMetrics_.calICUProductivity(icus.get(k).ICUMetrics_.getAllICUTasks());
            prod = tempICU.ICUMetrics_.getICUProductivity();
            
            acks = tempICU.ICUMetrics_.getICUAcks();
            reqs = tempICU.ICUMetrics_.getICUReqs();
            
            tempICU.ICUMetrics_.calICUwillingnessConf(icus.get(k));
            willconf = tempICU.ICUMetrics_.getICUWillingnessConf();

            tempICU.ICUMetrics_.calICUPerformanceTrustScore();
            pt = tempICU.ICUMetrics_.getICUPerformanceTrustScore();

            tempICU.ICUMetrics_.calICUsocioTechnicalTrust();
            socioTechnicalTrust = tempICU.ICUMetrics_.getICUSocioTechnicalTrust();

            tempICU.ICUMetrics_.setTotalAssignedTasks(totaltemp);
            tempICU.ICUMetrics_.setSuccDelTasks(sucdelt);
            tempICU.ICUMetrics_.setTotalDelTasks(totaldelt);
            tempICU.ICUMetrics_.setSuccessRate(sucr);
            //tempICU.ICUMetrics_.setSuccNonDelTasks(sucnondelt);
            tempICU.ICUMetrics_.setICUInvocationnr(invocnr);
            tempICU.ICUMetrics_.setDelegatedSuccessRate(delsucr);
            tempICU.ICUMetrics_.setNonDelSuccessRate(nondelsucr);
            tempICU.ICUMetrics_.setICUReliability(reliab);
            tempICU.ICUMetrics_.setICUAcks(acks);
            tempICU.ICUMetrics_.setICURequests(reqs);
            tempICU.ICUMetrics_.setWillingnessConf(willconf);
            tempICU.ICUMetrics_.setICUPerformanceTrustScore(pt);
            tempICU.ICUMetrics_.setICUProductivity(prod);
            tempICU.ICUMetrics_.setSocioTechnicalTrust(socioTechnicalTrust);

            tempicus.add(tempICU);

            metricsList.add(tempICU.ICUMetrics_);
            setMetricList(metricsList);
            // System.out.println(icus.get(k).getICUId()+indent+indent+icus.get(k).ICUMetrics_.getAllICUTasks().size()+indent+indent+String.format("%.4f",tempICU.ICUMetrics_.getMCTS())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUPerformanceTrustScore())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUSocioTechnicalTrust())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUProductivity())+indent+indent+icus.get(k).ICUMetrics_.getSuccessfulICUTaskQueue().size());    
        }
        //tempicus.getICUPool();
        //System.out.print("\n");*/

//        System.out.println("No Of Actions: " + listOfActions.size());
//        for (Action action : listOfActions){
//            System.out.println(action.getActionID() + " - " + action.getIncomming() + " - " + action.getOutgoing());
//        }
//        
        //process actions
        DirectedAcyclicalGraph dag = new DirectedAcyclicalGraph();
        dag.setListOfActions(listOfActions);
        dag.setListOfParallelGateways(listOfParallelGateways);

        icuProcess = new SLAChangeProcess( null, dag);
        return tempicus;
    }
    
    
    public SLAChangeProcess getICUProcess(){
        return icuProcess;  
    }
    
    private Action makeActionForProcess(ICUTask iCUTask){
        
        Action action = new Action(String.valueOf(iCUTask.getTaskId()), iCUTask.getTaskOwner().getICUName());
        return action;
    }
    
    //Delegate tasks for which parameter-change requests are made, e.g., cost.
    private static void negotiateSLAPar(ICUList icus) throws IOException {
        
        // The two methods take parameter changes as input.
        setICUCostperSkillReq();
        setParameterChangesReq();
        
        for (int k = 0; k < taskList.size(); k++) {
            tempTask = taskList.get(k);
            currentICU = tempTask.getTaskOwner();
            
            //Run through all ICUs in the ranked list of ICUs.
            outerloop:
            for (int b = 0; b < icus.size(); b++) {
             
                icutemp = icus.get(b);
                //I skip here the ICU to which an ICU is assigned
                if ((tempTask.getTaskOwner().getICUId()!= icutemp.getICUId()) && ((!currentSCU.contains(icutemp)))) {
                //{
                    innerloop:
                    for (Map.Entry<String, Double> entry : skillcost.entrySet()) {
                        
                        /*Find the first ICU with the required skill-type and the newly-changed cost. 
                        Delegate the task for which the cost is changed if ICU is found, if not leave it with the old ICU.*/
                        if ((tempTask.getSkillReq().equals(icutemp.getICUSkill())) && (entry.getKey().equals(icutemp.getICUSkill())) && (entry.getValue() > icutemp.getICUCostPerTask()) && (entry.getValue() < currentICU.getICUCostPerTask()) && (entry.getValue() != 0)) {
                
                            //Metrics updates: currentICU is the old ICU, icutemp is the ICU to which the task is delegated...
                            icutemp.updateICUTaskQueue(tempTask);
                            icutemp.ICUMetrics_.updateICUTaskQueue(tempTask);
                            currentICU.ICUMetrics_.removeTask(tempTask);
                            currentICU.failedTasks_++;
                    
                            acks=icutemp.ICUMetrics_.getICUAcks();
                            reqs=icutemp.ICUMetrics_.getICUReqs();
                            icutemp.ICUMetrics_.updateReceivedReqs(acks);
                            icutemp.ICUMetrics_.updateSentAcks(reqs);
                    
                            icutemp.ICUMetrics_.updateDelegatedTasks(tempTask);
                            icutemp.ICUMetrics_.updateReceivedReqs(acks);
                            icutemp.ICUMetrics_.updateSentAcks(reqs);
                            
                            icutemp.ICUMetrics_.setICUInvocationnr(invocnr);
                            //icutemp.ICUMetrics_.setICURequests(reqs);
                            //icutemp.ICUMetrics_.setICUAcks(acks);

                            //Add the new ICU to the SCU.
                            currentSCU.addICU(icutemp, currentSCU); //add(icu);

                            System.out.println();
                            System.out.print("Task " + tempTask.getTaskId() + " delegated from ICU with id " + tempTask.getTaskOwner().getICUId() + "and cost " + tempTask.getTaskOwner().getICUCostPerTask() + " and skill " + tempTask.getTaskOwner().getICUSkill());
                            tempTask.setTaskState(9);

                            //Set the new ICU as the owner of the task that was delegated.
                            tempTask.setTaskOwner(icutemp);
                            
                            System.out.println(" to ICU with id " + icutemp.getICUId() + " and new cost " + tempTask.getTaskOwner().getICUCostPerTask() + " " + entry.getValue() + " skill " + icutemp.getICUSkill() + " " + icutemp.getICUCostPerTask());
                            icutemp.ICUMetrics_.getAllICUTasks().getTaskwithId(tempTask.getTaskId()).taskDelegated = true;
                            break outerloop;
                        } //break;
                                //else{
                                //System.out.println("No ICU accepted the cost value, we cant adapt the SCU according to cost...assigning tasks according to skill...");
                                //}
                                //break;        
                    }

                            //break;
                        //}
                }
                    //break
            } //end icu loop
                //}
        }//end task loop

    }
    
    //Delegate tasks if the queue time reaches a pre-defined threshold value.
    private static void timeBasedDelegations(ICUList icus) throws IOException {
         for (min = 0; min < 300; min++) {
                for (int j = 0; j < taskList.size(); j++) {
                    tempTask = taskList.get(j);
                    //tempTask.taskDelegated=false;
                    tempTime = tempTask.getTaskInQueue();
                    
                    //Check if the time that the time has waited in an ICUs queue reached a threshold.
                    if (tempTime == min && (tempTime == tempTask.getTaskDeadline() / 2)) {

                        currentICU = tempTask.getTaskOwner();

                        System.out.println();
                        System.out.println("Task waiting-time for task with id " + tempTask.getTaskId() + " reached the pre-set threshold, delegating task...");

                        //Loop through ICUs in the ranked list to find a suitable ICU to delegate the task to.
                        for (int a = 0; a < icus.size(); a++) {
                            icutemp = icus.get(a);
                      
                            
                            //Skip the ICU from which we want the delagate the task to.
                            if (tempTask.getTaskOwner().getICUId() != icutemp.getICUId()) {
                                
                                //Check if another ICU has the same skill-type as the task to be delegated, delegate to the new ICU if the skill-types match.
                                if (tempTask.getSkillReq().equals(icutemp.getICUSkill())) {
                                    //icutemp.updateICURequests();
                                    //icutemp.updateICUAcks();
                                    icutemp.updateICUTaskQueue(tempTask);

                                    icutemp.ICUMetrics_.updateICUTaskQueue(tempTask);

                                    currentICU.ICUMetrics_.removeTask(tempTask);
                                    currentICU.failedTasks_++;

                                    icutemp.ICUMetrics_.updateDelegatedTasks(tempTask);
                                    acks=icutemp.ICUMetrics_.getICUAcks();
                                    reqs=icutemp.ICUMetrics_.getICUReqs();
                                    icutemp.ICUMetrics_.updateReceivedReqs(acks);
                                    icutemp.ICUMetrics_.updateSentAcks(reqs);
                                     //acks++;
                                     //reqs++;
                                    //icutemp.ICUMetrics_.setICURequests(reqs);
                                    //icutemp.ICUMetrics_.setICUAcks(acks);
                                    /* Check if the ICU to which the new task is assigned, already belongs to the SCU.
                                    If it doesn't, include it in the SCU/add the new ICU to the SCU. */
                                    if ((!currentSCU.contains(icutemp))) {
                                        currentSCU.addICU(icutemp, currentSCU); //add(icu);
                                    }

                                    System.out.println("Task " + tempTask.getTaskId() + " delegated from ICU with id " + tempTask.getTaskOwner().getICUId() + " to ICU with id " + icutemp.getICUId());
                                    tempTask.setTaskState(9);
                                    
                                    //Set the new ICU as the owner of the task that was delegated.
                                    tempTask.setTaskOwner(icutemp);
                                    
                                    //Note that the task was delegated.
                                    icutemp.ICUMetrics_.getAllICUTasks().getTaskwithId(tempTask.getTaskId()).taskDelegated = true;

                                    break;
                                }
                            }
                            //break
                        }
                    }
                }
            }//currentSCUMembers();
    }
    
    //Store the appropriate skill and the corresponding newly-changed cost requirements for tasks.
    public static void setICUCostperSkillReq() {
        skillCostreq = new ArrayList<>();
        skillcost = new HashMap<>();
        costchange = 0.0;
        propTemp = (int) (Math.random() * 5);
        //randCost=prop.ICUcostPerTask[propTemp];
        for (ICUTask t : taskList) {
            if (!skillCostreq.contains(t.getSkillReq())) {
                skillCostreq.add(t.getSkillReq());
            }
        }
        for (Iterator<String> it = skillCostreq.iterator(); it.hasNext();) {
            skilltemp = it.next();
            skillcost.put(skilltemp, costchange);
        }
    }

    //TODO add other parameter changes with client requests...
    //Changing an SLA in terms of cost.
    public static void setParameterChangesReq() throws IOException {
        Object value = 0;
        System.out.println("Do you want to change the fee for the following skills: ");
        for (int i = 0; i < skillCostreq.size(); i++) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println(skillCostreq.get(i).toString());
            System.out.println("Y/N: ");
            String answer = br.readLine();
            if (answer.equalsIgnoreCase("N")) {
                System.out.println("The fee for skill: " + skillCostreq.get(i).toString() + " not changed");

                break;
            } else if (answer.equalsIgnoreCase("Y")) {
                System.out.println("Enter a (double) number if you want to change the fee for work with the skill: " + skillCostreq.get(i).toString());
                try {
                    double input = Double.parseDouble(br.readLine());
                    value = input;
                    skillcost.put(skillCostreq.get(i).toString(), (Double) value);

                } catch (NumberFormatException invalidnr) {
                    System.err.println("Invalid Number Format!");
                }

                System.out.println("The fee for skill: " + skillCostreq.get(i).toString() + " changed to " + value);

            } else {
                System.out.println("Wrong input, try again ");
                i--;
            }
        }//end for
    }//end method

    public static void setMetricList(MetricsList metricsList) {
        SCURun_SLOChanges.metricsList = metricsList;
    }

    public MetricsList getMetricList() {
        return metricsList;
    }


    public int getTotalTime() {
        for (int t = 0; t < taskList.size(); t++) {
            currentTask = taskList.get(t);
            totalTime = totalTime + currentTask.getTaskTime();
        }
        return totalTime;
    }
    
    /*Calculate SCU metrics based on ICU metrics and print the ICUs that are members of the SCU, along with their metric values.
    Also print SCU metric values.*/
    public void currentSCUMembers() throws IOException {
        //iculistcurrent holds ICUs that are members in the SCU
        iculistcurrent = new ICUList();
        metricsscu = new MetricsSCU();
        iculistcurrent = currentSCU.getSCUMembers();
        
        String indent = "    ";
        System.out.println();
        System.out.println("========== OUTPUT: CURRENT SCU Members ==========");
        System.out.println("ICU ID" + indent + "alltasks" + indent + "mcts" + indent + "ptrust" + indent + "stt" + indent + indent + "succ tasks" + indent + "success rate");

        for (int j = 0; j < currentSCU.getSCUMembers().size(); j++) {
            System.out.println();
            icuscu = currentSCU.getSCUMembers().get(j);
            System.out.println(icuscu.getICUId() + indent + indent + icuscu.ICUMetrics_.getAllICUTasks().size() + indent + indent + String.format("%.4f", icuscu.ICUMetrics_.getMCTS()) + indent + String.format("%.4f", icuscu.ICUMetrics_.getICUPerformanceTrustScore()) + indent + String.format("%.4f", icuscu.ICUMetrics_.getICUSocioTechnicalTrust()) + indent + indent + indent + icuscu.ICUMetrics_.getSuccessfulICUTaskQueue().size() + indent + indent + String.format("%.4f", icuscu.ICUMetrics_.getICUSuccessRate()));
        }
        metricsscu.calProductivity(currentSCU);
        metricsscu.calPerformanceTrustScore(currentSCU);
        metricsscu.calSocialTrustScore(currentSCU);
        metricsscu.calSocioTechnicalTrust();
        metricsscu.calTotalSCUTasks(currentSCU);
        metricsscu.calTotalSCUDelegatedTasks(currentSCU);
        totaldelinvoc = totaldelinvoc + metricsscu.totalscudelegatedtasks_;
        System.out.println();
        System.out.println("========== SCU TRUST-BASED METRICS RESULTS ==========");
        System.out.println("SCU Id: " + currentSCU.getSCUId() + " Productivity: " + String.format("%.4f", metricsscu.productivitySCU_) + " Performance trust: " + String.format("%.4f", metricsscu.technicalTrustSCU_) + " Social trust: " + String.format("%.4f", metricsscu.socialTrustSCU_) + " Socio-technical trust: " + String.format("%.4f", metricsscu.socioTechnicalTrustSCU_));
        System.out.println("SCU total tasks: " + metricsscu.totalscutasks_ + " SCU invocation delegated tasks: " + metricsscu.totalscudelegatedtasks_ + " total delegated tasks " + totaldelinvoc);
        System.out.println("Total tasks time " + totalTime);
    }

}
