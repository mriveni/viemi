/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import dsg.infosys.agreementmodel.SSLA;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author Mirela Riveni 
 */
public class SCU extends ICUList implements ElasticityOperations{
    private int SCUId_;
    //private String SCUName_;
    //private String SCUDescription_;
    public  static int SCUCount;
    private static int SCUState_;
    private ICUList ICUlist_;
    private TaskQueue TaskQueue_;
    
    private SSLA scuSLA;
    
    private double SCUCost_;
    //private final ListIterator<ICU> memberIterator = ICUlist_.listIterator();
    public Metrics ICUMetrics = new Metrics();
    
    //stores only states not related to ICUids
    ArrayList<Integer> memberStatesList = new ArrayList<Integer>(); 
    int memberState;
    int idleMembers;
    int activeMembers;
    int suspendedMembers;
    int stoppedMembers;
    
    
    //constant attributes regarding SCU State
    public static final int SCUIDLE = 5; 
    public static final int SCUACTIVE = 6;  
    //public static final int SCUSUSPENDED = 7;
    //public static final int SCURESUMED = 8;
    public static final int SCUSTOPPED = 9;
    
    
    //constructors
    public SCU(){
   
    }
    //give the default value of 6 when initiating the SCU with this constructor to have an active SCU from start
    public SCU(int SCUId, int SCUState){
      SCUId_=SCUId;
      SCUState_=SCUState;
      SCUCount++;
    }   
    
    //setter and update methods
    public void setSCUMembers(ICUList ICUlist){
        ICUlist_=ICUlist;
    }
     
    public void setSCUCost(double SCUCost){
            SCUCost_=SCUCost;
    }
    
    public void setActiveICUs(ICUList ICUlist){
        ICUlist_=ICUlist;
    }
    public void setSuspendedICUs(ICUList ICUlist){
        ICUlist_=ICUlist;
    }
    public void setStoppedICUs(ICUList ICUlist){
        ICUlist_=ICUlist;
    }
           
    
    public void updateSCUState(ICUList ICUlist){
       
       for (int i = 0; i < ICUlist_.size(); i++) {
	    memberState=ICUlist_.get(i).getICUState();
            memberStatesList.add(memberState);
            switch(memberState){
                case 0:
                    idleMembers++;
                    break;
                case 1:
                    activeMembers++;
                    break;
                case 2:
                    suspendedMembers++;
                    break;
                case 4:
                    stoppedMembers++;
                    break;
                default:
                    break;
            }
       }
       if(activeMembers!=0)
       {
        if ((idleMembers>activeMembers)||((idleMembers+suspendedMembers)> activeMembers))
        {
            SCUState_=5;
        }
        else
        {
            SCUState_=6;
        }
       }
       else if((idleMembers>stoppedMembers)||((idleMembers+suspendedMembers)> stoppedMembers))
       {
        SCUState_=5;
       }
       else 
       {
        SCUState_=9;
       }
    }
        
    //getter methods 
    public int getSCUId(){
        return SCUId_;
    }
    
    public ICUList getSCUMembers(){
        return ICUlist_;
    }
     public double getSCUCost(){
        return SCUCost_;
    }
    
    public ICUList getActiveSCUs()
    {
        return ICUlist_;
    }
    
    public ICUList getSuspendedSCUs(){
        return ICUlist_;
    }
    
    public ICUList getExcludedSCUs(){
        return ICUlist_;
    }
    
    public int getSCUState(){
       return SCUState_;
    }
    
    // get the state of an SCU in a String type
    public static String getSCUStateString(int SCUState){
        String SCUStateString = null;
        switch(SCUState){
            case SCUIDLE:
                 SCUStateString="SCUIdle";
                 break;
            case SCUACTIVE:
                 SCUStateString="SCUActive";
                break;
            //case SCUSUSPENDED:
              //   SCUStateString="SCUSuspended";
                // break;
            //case SCURESUMED:
              //   SCUStateString="SCUResumed";
                // break;
            case SCUSTOPPED:
                 SCUStateString="SCUStopped";
                 break;
            default:
                 break;
        }
        return SCUStateString;
    }
    
       
   @Override
    public void addICU(ICU icu, SCU scu) {
      scu.add(icu);
      //scu.setSCUMembers(ICUlist_);
 
    }

    @Override
    public void suspendICU(ICU icu, SCU scu) {
        
    }

    @Override
    public void resumeICU(ICU icu, SCU scu) {
       
    }

    @Override
    public void excludeICU(ICU icu, SCU scu) {
        
    }

    @Override
     public LinkedList<ICU> reserveICUs(ICUList reserveICUs) {
       return ICUlist_;
    }

    @Override
     public LinkedList<ICU> getAllICUsInSCU(SCU scu) {
        return ICUlist_;
    }
    


 
}
