/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.agreementmodel;

import dsg.infosys.slaparameters.SLAParameter;
import java.util.ArrayList;

/**
 *
 * @author Mirela Riveni
 */
public interface HSLA {
    
  public ArrayList<Integer> getSLAParties();  
  public SLAParameter getSLAParameters();
  
}
