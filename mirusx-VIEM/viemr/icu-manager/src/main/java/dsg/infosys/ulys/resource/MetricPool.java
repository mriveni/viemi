/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.ulys.resource;

import dsg.infosys.basemodel.MetricsList;
import dsg.infosys.demo_algorithms.STTAlgorithm_ALLICUS_oneSCUInvocation;
import java.io.IOException;


public class MetricPool {

    public static MetricsList initMetricsList() throws Exception{
       
        STTAlgorithm_ALLICUS_oneSCUInvocation algosmth = new STTAlgorithm_ALLICUS_oneSCUInvocation();
        algosmth.algo();
        algosmth.setMetricList(algosmth.metricsList);
        MetricsList metricsi = algosmth.getMetricList();
        return metricsi;
    }
    
}
