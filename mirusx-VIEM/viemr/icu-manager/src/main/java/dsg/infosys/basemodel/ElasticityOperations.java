/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import java.util.LinkedList;

/**
 *
 * @author Mirela Riveni
 */

public interface ElasticityOperations {
    
    
    /***
     * 
     * @param ICUId of the ICU to be included in the SCU when the SCU is extended.
     * @param SCUId of the SCU that the ICU belongs to.
     * When implementing the method set the state of the ICU to ACTIVE.
     */
    public void addICU(ICU icu, SCU scu);
    
    
    /***
     * 
     * @param icu to be stopped but not excluded from the ICU.
     * @param scu that the ICU belongs to.
     * When implementing the method set the state of the ICU to SUSPENDED. 
     */
    public void suspendICU(ICU icu, SCU scu); 
    
    
    /***
     * 
     * @param ICUId of the ICU in IDLE or SUSPENDED state and that resumes work when tasks are assigned to it.
     * @param SCUId of the SCU that the ICU belongs to.
     * When implementing the method set the ICU state to RESUMED and after a time period to ACTIVE.
     */
    public void resumeICU(ICU icu, SCU scu);
    
    
    /***
     * 
     * @param ICUId of the ICU that is in state IDLE and has no tasks assigned, according to a scheduler.
     * @param SCUId of the SCU that the ICU belongs to.
     * When implementing the method set the ICU state to STOPPED and exclude it from the SCU.
     */
    public void excludeICU(ICU icu, SCU scu);
    
    
    /***
     * 
     * @param reserveICUs ICU instances
     * @return returns a list of appropriate ICUs for a certain task. ICUs might be in/out of the specific SCU.
     */
    
    public LinkedList<ICU> reserveICUs(ICUList reserveICUs);
    
    
    /***
     * 
     * @param SCUId of the SCU for which we request the ICUs belonging to it.
     * @return a list of ICUs representing the specific SCU.
     */
    public LinkedList<ICU> getAllICUsInSCU(SCU scu);

}

