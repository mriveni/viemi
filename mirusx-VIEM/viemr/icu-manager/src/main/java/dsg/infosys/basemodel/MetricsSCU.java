/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;


import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Mirela Riveni
 */

public class MetricsSCU {
    
    SCU scu = new SCU();
    ICUList icuInSCU = new ICUList();
    int scusize;
    public double icupt, sumpt, icumcts, summcts, icustt, w, css, sumproductivity, icuprod;
    public double technicalTrustSCU_;
    public double socialTrustSCU_;
    public double socioTechnicalTrustSCU_;
    double random;
    public  double productivitySCU_;
    public int totalscutasks, totalscudelegatedtasks;
    public int totalscutasks_, totalscudelegatedtasks_;
 
    public void generateRandom(){
       random=ThreadLocalRandom.current().nextDouble(0.5, 1);
  
    }
    public double getRandom(){
       return random;
    }
     
    public double calProductivity(SCU scu){
        icuInSCU=scu.getSCUMembers();
        scusize=icuInSCU.size();
        for (ICU icu: icuInSCU){
            icuprod=icu.ICUMetrics_.getICUProductivity();
            sumproductivity=sumproductivity+icuprod;
        }
        productivitySCU_=sumproductivity/(double)scusize;
        return productivitySCU_;
    } 
      
    public int calTotalSCUTasks(SCU scu){
    icuInSCU=scu.getSCUMembers();
    scusize=icuInSCU.size();
    for (ICU icu: icuInSCU){
        totalscutasks=icu.ICUMetrics_.getAllICUTasks().size();
            totalscutasks_=totalscutasks_+totalscutasks;}
    return totalscutasks_;
    } 
    
    public int calTotalSCUDelegatedTasks(SCU scu){
    icuInSCU=scu.getSCUMembers();
    scusize=icuInSCU.size();
    for (ICU icu: icuInSCU){
        totalscudelegatedtasks=icu.ICUMetrics_.getTotalDelTasks();
            totalscudelegatedtasks_=totalscudelegatedtasks_+totalscudelegatedtasks;}
    return totalscudelegatedtasks_;
    } 
        
    public double calPerformanceTrustScore(SCU scu){
        icuInSCU=scu.getSCUMembers();
        scusize=icuInSCU.size();
        for (ICU icu: icuInSCU){
            icupt=icu.ICUMetrics_.getICUPerformanceTrustScore();
            sumpt=sumpt+icupt;
        }
        technicalTrustSCU_=sumpt/(double)scusize;
        return technicalTrustSCU_;
    }  
    
    
     
     public double calSocialTrustScore(SCU scu){
        icuInSCU=scu.getSCUMembers();
        
        scusize=icuInSCU.size();
        for (ICU icu: icuInSCU){
            //generateRandom();
            icumcts=icu.ICUMetrics_.getMCTS();
            //css=getRandom();
            //w=css;
            //summcts=((w*icumcts)); //+((1-w)*css));
            socialTrustSCU_=socialTrustSCU_+icumcts;
        }
        socialTrustSCU_=(socialTrustSCU_/(double)scusize);
        return socialTrustSCU_;
    }
    
    
      public double calSocioTechnicalTrust(){
   
        socioTechnicalTrustSCU_=(technicalTrustSCU_+socialTrustSCU_)/2;
        return socioTechnicalTrustSCU_;
    }
        
}
