/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.slaprocess.demo;

import java.util.ArrayList;
import java.util.List;
import dsg.infosys.common.entity.eda.elasticprocess.Action;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.eda.elasticprocess.DirectedAcyclicalGraph;
import dsg.infosys.common.entity.eda.elasticprocess.ParallelGateway;
import dsg.infosys.common.entity.eda.elasticprocess.Task;
import dsg.infosys.slaprocess.basemodel.TaskFindingNewICU;
import dsg.infosys.slaprocess.basemodel.TaskICUAssignment;
import dsg.infosys.slaprocess.basemodel.TaskSLANegotiation;
import dsg.infosys.slaprocess.basemodel.TaskUpdateSLAs;
import dsg.infosys.slaprocess.basemodel.TaskWorkingWithoutChangesICU;


public class SLAChangeProcessModel {
    public SLAChangeProcess getSampleProcess() {
        List<Action> listOfActions = new ArrayList<>();
        List<Task> listOfTasks = new ArrayList<>();

        {
            String actionId = "t1";
            String actionName = "task1";
            String incomming = null;
            String outgoing = "t2";

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
            
            TaskUpdateSLAs task = new TaskUpdateSLAs();
            listOfTasks.add(task);
            
        }

        {
            String actionId = "t2";
            String actionName = "task2";
            String incomming = "t1";
            String outgoing = "g1";

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
            
            TaskSLANegotiation task = new TaskSLANegotiation();
            listOfTasks.add(task);
        }

        {
            String actionId = "t3";
            String actionName = "task3";
            String incomming = "g1";
            String outgoing = null;

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
            
            TaskWorkingWithoutChangesICU task = new TaskWorkingWithoutChangesICU();
            listOfTasks.add(task);
        }

        {
            String actionId = "t4";
            String actionName = "task4";
            String incomming = "g1";
            String outgoing = "t5";

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
            
            TaskFindingNewICU task = new TaskFindingNewICU();
            listOfTasks.add(task);
        }
        
        {
            String actionId = "t5";
            String actionName = "task5";
            String incomming = "t4";
            String outgoing = null;

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
            
            TaskICUAssignment task = new TaskICUAssignment();
            listOfTasks.add(task);
        }

        List<ParallelGateway> listOfParallelGateways = new ArrayList<>();

        {

            String gatewayId = "g1";
            
            List<String> listOfIncommings = new ArrayList<String>() {
                {
                    add("t2");
                }
            };
            
            List<String> listOfOutgoings = new ArrayList<String>(){
                {
                    add("t3");
                    add("t4");
                }
            };
            
            
            ParallelGateway parallelGateway = new ParallelGateway(gatewayId, listOfIncommings, listOfOutgoings);
            listOfParallelGateways.add(parallelGateway);
        }
        
      
        
        
        

        DirectedAcyclicalGraph dag = new DirectedAcyclicalGraph();
        dag.setListOfActions(listOfActions);
        dag.setListOfParallelGateways(listOfParallelGateways);

        SLAChangeProcess sLAChangeProcess = new SLAChangeProcess( listOfTasks, dag);

        return sLAChangeProcess;
    }
}
