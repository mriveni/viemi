/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.demo_algorithms;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import javax.xml.bind.JAXBException;
import dsg.infosys.ranking.AHPQoSRanking;
import dsg.infosys.basemodel.ICU;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.basemodel.ICUTask;
import dsg.infosys.basemodel.Metrics;
import dsg.infosys.basemodel.MetricsList;
import dsg.infosys.basemodel.MetricsSCU;
import dsg.infosys.basemodel.SCU;
import dsg.infosys.basemodel.TaskPool;
import dsg.infosys.basemodel.TaskQueue;

/**
 *
 * @author Mirela Riveni
 */
public class SCURun_TrustModelBased {    
private static ICU icu, icutemp, currentICU, icuscu, changeicu;    
private static ICUTask task, tempTask;
public static ICUList icus = new ICUList();   
public static ICUList icus_ = new ICUList();   
public static ICUList tempicus=null;
public static ICUList iculistcurrent;
public static TaskQueue taskList, allicutasks, dynamicicutasklist, currentICUtaskList;
public static TaskPool taskPool;
static final AHPQoSRanking selectedicus=new AHPQoSRanking();
private static SCU currentSCU;
public static ICUList icuPool, icusnew, icusloop;
public static int[] minutes = new int[300]; 
private static int queueTime, tempTime;
private static ICU tempICU;
private static Metrics metrics;
private static MetricsSCU metricsscu;
public ICUList icuinvocationlist, icuinvocations, currentinvocations, icutempinvocations, invocations;

public MetricsList metricsList;

public static int totaltasks=0, totaldelt, totalnondelt, sucdelt, sucnondelt, totaltemp, totaldelinvoc=0;
double sucr, delsucr, nondelsucr,  willconf, reliab, prod, pt, mcts, socioTechnicalTrust;
boolean will;
DecimalFormat df = new DecimalFormat("#.0000"); 

public static TaskPool generateTasks(){
    
    taskPool=new TaskPool();
    taskPool.createHardcodedTaskPool();
    
    return taskPool;
}

public SCURun_TrustModelBased() {
    this.metricsList = new MetricsList();
}
    
//generate ICUs with an AHP-based ranking algorithm using the 'AHPQoSRanking.java' class in the 'ranking' package     
public ICUList generateICUs() throws JAXBException, IOException, FileNotFoundException, URISyntaxException{
    icus_=selectedicus.getRankedICUs();
    icus_.setICUs(new ICUList());
    icus_.setICUs(selectedicus.getRankedICUs());
    return icus_;
} 

public void clearICUTrustScores(ICUList icus){
    
    sucr=0.0; delsucr=0.0; nondelsucr=0.0;  willconf=0.0; reliab=0.0; prod=0.0; pt=0.0; mcts=0.0; socioTechnicalTrust=0.0; totaldelt=0; totaltemp=0; sucnondelt=0;
    for (int j=0; j<currentSCU.size();j++){
        changeicu=new ICU();
        changeicu=currentSCU.get(j);
        changeicu.ICUMetrics_.setTotalAssignedTasks(totaltemp);
        changeicu.ICUMetrics_.setSuccDelTasks(sucdelt);
        changeicu.ICUMetrics_.setTotalDelTasks(totaldelt);
        changeicu.ICUMetrics_.setSuccessRate(sucr);
        changeicu.ICUMetrics_.setSuccNonDelTasks(sucnondelt);
        changeicu.ICUMetrics_.setICUInvocations(icutempinvocations);
        changeicu.ICUMetrics_.setDelegatedSuccessRate(delsucr);
        changeicu.ICUMetrics_.setNonDelSuccessRate(nondelsucr);
        changeicu.ICUMetrics_.setICUReliability(reliab);
        changeicu.ICUMetrics_.setWillingnessConf(willconf);
        changeicu.ICUMetrics_.setICUPerformanceTrustScore(pt);
        changeicu.ICUMetrics_.setICUProductivity(prod);
        changeicu.ICUMetrics_.setSocioTechnicalTrust(socioTechnicalTrust);
    }
} 

//public static void main(String[] args) throws JAXBException, IOException, FileNotFoundException, URISyntaxException {
 public ICUList algo(ICUList icus) throws IOException{
    System.out.println("ASSIGNING A NEW BAG OF TASKS TO THE SCU...");
    currentSCU = new SCU();
    taskList=generateTasks().getTaskPool();
 
    task = new ICUTask();
    tempTask = new ICUTask();
    icu = new ICU();
    icutemp = new ICU();
    currentICU = new ICU();
    currentICUtaskList=currentICU.getICUTaskQueue();
    int min = 0;
   
    for (int i=0; i<taskList.size(); i++){
        queueTime=(int)(Math.random()* (240 - 20) + 20);
        task=taskList.get(i);
        
        //hardcode taskQueueTime
        if (queueTime<task.getTaskDeadline()){
            task.setTaskInQueue(queueTime);
        }
        else {
            task.setTaskInQueue(task.getTaskDeadline()-5);
        }
        
        //assign tasks to ICUs with matching skills
        for (int j=0; j<icus.size();j++){
            totaltasks=0;
            icu=icus.get(j);
            
            if(task.getSkillReq().equals(icu.getICUSkill())){
           
            icu.totaltasks_++;
            will=icu.getWillingness();
            
            icu.getICUMetrics().updateICUTaskQueue(task);
            icu.ICUMetrics_.calICUInvocations(icu); 
            icu.ICUMetrics_.setTotalAssignedTasks(icu.getICUTaskQueue().size());
            task.setTaskOwner(icu);
            if((!currentSCU.contains(icu))){
                currentSCU.addICU(icu, currentSCU); //add(icu);
            }
            task.setTaskState(6);
            System.out.println("Task with id "+task.getTaskId()+"is assigned to ICU with id"+icu.getICUId());
            //System.out.println("task queue time "+task.getTaskInQueue()+" deadline "+task.getTaskDeadline());
            break;
           }
        }
           //else 
           //System.out.println("No appropriate ICU found for this task.");
           //System.out.print(icu.getICUId());
           //System.out.print(task.getTaskId());
    }
    
    
    int a;
    //FCFS delegations
    for (min=0; min<300; min++){ 
        for(int j=0; j<taskList.size(); j++){
            tempTask=taskList.get(j);
            //tempTask.taskDelegated=false;
            tempTime=tempTask.getTaskInQueue();
            if(tempTime==min && (tempTime == tempTask.getTaskDeadline()/2)){
                currentICU=tempTask.getTaskOwner();
                System.out.println();
                System.out.println("Task waiting-time for task with id "+tempTask.getTaskId()+" reached the pre-set threshold, delegating task...");
                //currentICU.ICUMetrics.setFailedTasks(currentICUtaskList);  
                for (a=0; a<icus.size(); a++){
                  
                    icutemp=icus.get(a);  
                       
                    if(tempTask.getTaskOwner().getICUId()!=icutemp.getICUId()){
                    if(tempTask.getSkillReq().equals(icutemp.getICUSkill())){
                          
                        icutemp.updateICUTaskQueue(tempTask);

                        icutemp.ICUMetrics_.updateICUTaskQueue(tempTask);
                        currentICU.ICUMetrics_.removeTask(tempTask);
                        currentICU.failedTasks_++;

                        icutemp.ICUMetrics_.updateDelegatedTasks(tempTask);
                       
             
                        if((!currentSCU.contains(icutemp))){
                            currentSCU.addICU(icutemp, currentSCU); //add(icu);

                        }
                             
                            
                        System.out.println("Task "+tempTask.getTaskId()+" delegated from ICU with id "+ tempTask.getTaskOwner().getICUId() + " to ICU with id "+ icutemp.getICUId());
                        tempTask.setTaskState(9);
                     
                        tempTask.setTaskOwner(icutemp);
                        icutemp.ICUMetrics_.getAllICUTasks().getTaskwithId(tempTask.getTaskId()).taskDelegated=true;
                                    
                        break;
                    }
                    }
                    //break
                } 
            }
        }
    }//currentSCUMembers();
   /*String indent = "    ";
   System.out.println();
   System.out.println("========== OUTPUT ==========");
   System.out.println("ICU ID" + indent + "alltasks" + indent + "mcts" + indent + "ptrust" + indent  + "stt"+indent+" productivity"+indent+"succ tasks"); //+
            //indent+ "total tasks"+indent+"product"+indent+indent+"scup"+indent+"scumcts");*/
   currentSCU.setSCUMembers(currentSCU);
   
    for (int k=0; k<currentSCU.size();k++){
    tempICU=currentSCU.get(k);
   
    tempICU.ICUMetrics_.setMICUid(tempICU.getICUId());
    tempICU.ICUMetrics_.calICUCollaborationScore();
    mcts=tempICU.ICUMetrics_.getMCTS();

    tempICU.ICUMetrics_.setMCTS(mcts);
      
    
    tempICU.ICUMetrics_.calNonDelTasks();
    tempICU.ICUMetrics_.calSuccessfulDelNonDelTasks(tempICU.ICUMetrics_.getAllICUTasks());
    
    sucdelt=tempICU.ICUMetrics_.getSuccDelTasks();
    sucnondelt=tempICU.ICUMetrics_.getSuccNonDelTasks();
    totaldelt=tempICU.ICUMetrics_.getTotalDelTasks();
    totaltemp= tempICU.ICUMetrics_.getAllICUTasks().size();


    totalnondelt=totaltemp-totaldelt;
   

    icutemp.ICUMetrics_.setTotalNonDelTasks(totalnondelt);
    icutemp.ICUMetrics_.setSuccNonDelTasks(sucnondelt);

    
    tempICU.ICUMetrics_.calDelNonDelSuccessRate(tempICU.ICUMetrics_.getAllICUTasks());
    
    
    delsucr=tempICU.ICUMetrics_.getDelegatedSuccessRate();
    nondelsucr=tempICU.ICUMetrics_.getNonDelSuccessRate();
    
    
    tempICU.ICUMetrics_.calSuccessRate(currentSCU.get(k).ICUMetrics_.getAllICUTasks());
    sucr=tempICU.ICUMetrics_.getICUSuccessRate();
    
    
    icutempinvocations=tempICU.ICUMetrics_.getICUInvocations();
    
    tempICU.ICUMetrics_.calICUreliability();
    reliab=tempICU.ICUMetrics_.getICUreliability();
    
    tempICU.ICUMetrics_.calICUProductivity(currentSCU.get(k).ICUMetrics_.getAllICUTasks());
    prod=tempICU.ICUMetrics_.getICUProductivity();
    
    
    tempICU.ICUMetrics_.calICUwillingnessConf(currentSCU.get(k));
    willconf=tempICU.ICUMetrics_.getICUWillingnessConf();
    
    
    
    tempICU.ICUMetrics_.calICUPerformanceTrustScore();
    pt=tempICU.ICUMetrics_.getICUPerformanceTrustScore();
    
    
    tempICU.ICUMetrics_.calICUsocioTechnicalTrust();    
    socioTechnicalTrust=tempICU.ICUMetrics_.getICUSocioTechnicalTrust();
    
    tempICU.ICUMetrics_.setTotalAssignedTasks(totaltemp);
    tempICU.ICUMetrics_.setSuccDelTasks(sucdelt);
    tempICU.ICUMetrics_.setTotalDelTasks(totaldelt);
    tempICU.ICUMetrics_.setSuccessRate(sucr);
    //tempICU.ICUMetrics_.setSuccNonDelTasks(sucnondelt);
    tempICU.ICUMetrics_.setICUInvocations(icutempinvocations);
    tempICU.ICUMetrics_.setDelegatedSuccessRate(delsucr);
    tempICU.ICUMetrics_.setNonDelSuccessRate(nondelsucr);
    tempICU.ICUMetrics_.setICUReliability(reliab);
    tempICU.ICUMetrics_.setWillingnessConf(willconf);
    tempICU.ICUMetrics_.setICUPerformanceTrustScore(pt);
    tempICU.ICUMetrics_.setICUProductivity(prod);
    tempICU.ICUMetrics_.setSocioTechnicalTrust(socioTechnicalTrust);
    tempicus.add(tempICU);
 
    metricsList.add(tempICU.ICUMetrics_);
    setMetricList(metricsList);
       // System.out.println(icus.get(k).getICUId()+indent+indent+icus.get(k).ICUMetrics_.getAllICUTasks().size()+indent+indent+String.format("%.4f",tempICU.ICUMetrics_.getMCTS())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUPerformanceTrustScore())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUSocioTechnicalTrust())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUProductivity())+indent+indent+icus.get(k).ICUMetrics_.getSuccessfulICUTaskQueue().size());    
    }
    
      
    return tempicus;
}


public static void main(String[] args) throws JAXBException, IOException, FileNotFoundException, URISyntaxException{

    SCURun_TrustModelBased sttalgo = new SCURun_TrustModelBased();
    tempicus = new ICUList();
   
    icusnew=new ICUList();
    icusloop=new ICUList();

    icusnew=sttalgo.generateICUs();

   
    sttalgo.algo(icusnew);
    sttalgo.currentSCUMembers();
    for(int i=0; i<10; i++){
        System.out.println();
        sttalgo.clearICUTrustScores(icusnew);
        sttalgo.algo(icusnew); 
        sttalgo.currentSCUMembers();
 
    }
    
}


public void setMetricList(MetricsList metricsList){
    this.metricsList=metricsList;
}

public MetricsList getMetricList(){
    return metricsList;
}

public void currentSCUMembers() throws IOException{
        
   iculistcurrent=new ICUList();

   metricsscu=new MetricsSCU();

   iculistcurrent=currentSCU.getSCUMembers();
   String indent = "    ";
   System.out.println();
   System.out.println("========== OUTPUT: CURRENT SCU Members ==========");
   System.out.println("ICU ID" + indent + "alltasks" + indent + "mcts" + indent + "ptrust" + indent  + "stt"+indent+" productivity"+indent+"succ tasks"+indent+"success rate");
   //System.out.print("ICUs in the current SCU are those with Ids: ");
   for(int j=0;j<currentSCU.getSCUMembers().size();j++){

       
        System.out.println();
        icuscu=currentSCU.getSCUMembers().get(j);
      

        System.out.println(icuscu.getICUId()+indent+indent+icuscu.ICUMetrics_.getAllICUTasks().size()+indent+indent+String.format("%.4f",icuscu.ICUMetrics_.getMCTS())+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUPerformanceTrustScore())+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUSocioTechnicalTrust())+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUProductivity())+indent+indent+indent+icuscu.ICUMetrics_.getSuccessfulICUTaskQueue().size()+indent+indent+String.format("%.4f",icuscu.ICUMetrics_.getICUSuccessRate()));          
   }
       metricsscu.calProductivity(currentSCU);  
       metricsscu.calPerformanceTrustScore(currentSCU);
       metricsscu.calSocialTrustScore(currentSCU);
       metricsscu.calSocioTechnicalTrust();
       metricsscu.calTotalSCUTasks(currentSCU);
       metricsscu.calTotalSCUDelegatedTasks(currentSCU);
       totaldelinvoc=totaldelinvoc+metricsscu.totalscudelegatedtasks_;
       System.out.println("========== SCU TRUST-BASED METRICS RESULTS ==========");
       System.out.println("SCU Id: "+currentSCU.getSCUId()+ " Productivity: " + String.format("%.4f",metricsscu.productivitySCU_) + " Performance trust: "+String.format("%.4f",metricsscu.technicalTrustSCU_ )+" Social trust: "+ String.format("%.4f",metricsscu.socialTrustSCU_ )+" Socio-technical trust: "+String.format("%.4f",metricsscu.socioTechnicalTrustSCU_));   
       System.out.println("SCU total tasks: "+metricsscu.totalscutasks_+" SCU invocation delegated tasks: "+metricsscu.totalscudelegatedtasks_+" total delegated tasks "+totaldelinvoc);
} 

}
