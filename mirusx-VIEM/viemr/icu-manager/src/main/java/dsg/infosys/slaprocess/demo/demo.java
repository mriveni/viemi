/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.slaprocess.demo;

import java.util.List;
import javax.xml.bind.JAXBException;
import test.dsg.infosys.process.engine.SampleProcess;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.runtime.ExecutionStep;
import dsg.infosys.demo_algorithms.SCURun_SLOChanges;
import dsg.infosys.slaprocess.utils.ProcessContext;
import dsg.infosys.slaprocess.utils.SLAProcessEngine;
import dsg.infosys.process.engine.dataelasticitycontroller.DEPExecutionPlanning;

/**
 *
 * @author junnguyen
 */
public class demo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        ProcessContext.init();
        ProcessContext.setsCURun_SLOChanges(new SCURun_SLOChanges());

        SLAChangeProcessModel sLAChangeProcessModel = new SLAChangeProcessModel();
        SLAChangeProcess sLAChangeProcess = sLAChangeProcessModel.getSampleProcess();
        
        SLAProcessEngine sLAProcessEngine = new SLAProcessEngine(sLAChangeProcess);
        sLAProcessEngine.execute();
        
        ProcessContext.getsCURun_SLOChanges().currentSCUMembers();
        
        
    }
    
}
