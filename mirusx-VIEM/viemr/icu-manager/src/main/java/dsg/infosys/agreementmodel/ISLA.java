/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.agreementmodel;

import dsg.infosys.slaparameters.SLAParameter;
import java.util.ArrayList;

/**
 *
 * @author Mirela Riveni
 */

//SLAs for Individual Compute Units (ICUs).
public class ISLA {
    
    private int id;
    private ArrayList<SLAParameter> listOfSLAParameters;
    private SLAProvider provider;
    private SLAConsumer consumer;

    public ISLA() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<SLAParameter> getListOfSLAParameters() {
        return listOfSLAParameters;
    }

    public void setListOfSLAParameters(ArrayList<SLAParameter> listOfSLAParameters) {
        this.listOfSLAParameters = listOfSLAParameters;
    }

    public SLAProvider getProvider() {
        return provider;
    }

    public void setProvider(SLAProvider provider) {
        this.provider = provider;
    }

    public SLAConsumer getConsumer() {
        return consumer;
    }

    public void setConsumer(SLAConsumer consumer) {
        this.consumer = consumer;
    }
      
    
}
