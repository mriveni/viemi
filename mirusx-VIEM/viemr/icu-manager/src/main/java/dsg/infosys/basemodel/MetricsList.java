/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.basemodel;

import dsg.infosys.demo_algorithms.STTAlgorithm_ALLICUS_oneSCUInvocation;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirela Riveni
 */
@XmlRootElement(name = "metricsList")
@XmlAccessorType(XmlAccessType.FIELD)
public class MetricsList extends LinkedList<Metrics> {

    public MetricsList() {
    }

}
