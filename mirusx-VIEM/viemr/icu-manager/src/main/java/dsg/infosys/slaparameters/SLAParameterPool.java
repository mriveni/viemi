/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.slaparameters;

import java.util.ArrayList;


public class SLAParameterPool {
    
    private ArrayList<SLAParameter> listOfSLAParameters;

    public SLAParameterPool() {
        listOfSLAParameters = new ArrayList<>();
        initializeSLAPool();
    }
    
    private void initializeSLAPool(){
        
        //here we need to get SLAParameter as Input and store the parameters in ISLA or SSLA
        SLAParameter successRate = new SLAParameter(1, "successRate", "%" );
        SLAParameter willingness = new SLAParameter(2, "willingness", "%" );
        SLAParameter trust = new SLAParameter(3, "trust", "%" );
        
        listOfSLAParameters.add(successRate);
        listOfSLAParameters.add(willingness);
        listOfSLAParameters.add(trust);
    }
    
    
    public SLAParameter getSLAParameterByName(String parameterName){
        
        for (SLAParameter sLAParameter : listOfSLAParameters) {
            if (sLAParameter.getParameterName().equals(parameterName)){
                return sLAParameter;
            }
        }
        
        return null;
    }
    
    public SLAParameter getSLAParameterById(int parameterId){
        
        for (SLAParameter sLAParameter : listOfSLAParameters) {
            if (sLAParameter.getParameterId()==parameterId){
                return sLAParameter;
            }
        }
        
        return null;
    }
    
    
    
    
    

}
