/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.slaparameters;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "SLAParameter")
@XmlAccessorType(XmlAccessType.FIELD)
public class SLAParameter {
    
    @XmlElement(name = "parameterId")
    int parameterId;
    
    @XmlElement(name = "parameterName")
    String parameterName;
    
    @XmlElement(name = "parameterUnit")
    String parameterUnit;
    
    @XmlElement(name = "parameterValue")
    Double parameterValue;

    public SLAParameter() {
    }

    public SLAParameter(int parameterId, String parameterName, String parameterUnit, Double parameterValue) {
        this.parameterId = parameterId;
        this.parameterName = parameterName;
        this.parameterUnit = parameterUnit;
        this.parameterValue = parameterValue;
    }

    public SLAParameter(int parameterId, String parameterName, String parameterUnit) {
        this.parameterId = parameterId;
        this.parameterName = parameterName;
        this.parameterUnit = parameterUnit;
    }

    public int getParameterId() {
        return parameterId;
    }

    public void setParameterId(int parameterId) {
        this.parameterId = parameterId;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterUnit() {
        return parameterUnit;
    }

    public void setParameterUnit(String parameterUnit) {
        this.parameterUnit = parameterUnit;
    }

    public Double getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(Double parameterValue) {
        this.parameterValue = parameterValue;
    }

    

}
