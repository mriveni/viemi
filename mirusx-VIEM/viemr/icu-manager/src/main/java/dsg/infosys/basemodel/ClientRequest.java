/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.basemodel;

import dsg.infosys.agreementmodel.ISLA;
import dsg.infosys.agreementmodel.SSLA;
import java.util.ArrayList;

/**
 *
 * @author junnguyen
 */
public class ClientRequest {
    
    private ArrayList<SSLA> listOfSSLAs;

    public ClientRequest() {
        listOfSSLAs = new ArrayList<>();
    }

    public ClientRequest(ArrayList<SSLA> listOfSSLAs) {
        this.listOfSSLAs = listOfSSLAs;
    }

    public ArrayList<SSLA> getListOfSSLAs() {
        return listOfSSLAs;
    }

    public void setListOfSSLAs(ArrayList<SSLA> listOfSSLAs) {
        this.listOfSSLAs = listOfSSLAs;
    }
    
    public void addSSLA(SSLA sSLA) {
        listOfSSLAs.add(sSLA);
    }
}
