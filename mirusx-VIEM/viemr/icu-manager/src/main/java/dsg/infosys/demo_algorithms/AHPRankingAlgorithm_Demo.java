package dsg.infosys.demo_algorithms;

import dsg.infosys.basemodel.ClientRequests;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import dsg.infosys.basemodel.ICU;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.basemodel.Matrix;
import static dsg.infosys.demo_algorithms.SCURun_SLOChanges.skillcost;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import dsg.infosys.utils.ICUListToFromXML;
import java.util.List;
import java.util.Scanner;
import javax.xml.bind.JAXBException;
import dsg.infosys.utils.ICUComp;



/**
 *
 * @author Mirela Riveni
 * An example of ranking with the AHP algorithm (without sub-characteristics) with hard-coded values.
 * Uses class: Matrix.java from the "basemodel" package
 */

public class AHPRankingAlgorithm_Demo{
private static double QoSr;
private static double offeredValue_, requestedValue_;
private static double overallSatisfaction;
private static final List<Double> overallSofR= new ArrayList<>();
private static final List<Double> overallSperR= new ArrayList<>();
private static double rating;
//private static final double[] requestedValues = new double[]{1.5, 2, 0.2};
private static ClientRequests cr = new ClientRequests();
private static final LinkedList requestedValues = new LinkedList();
private static double[] offeredValues = new double[]{};
private static ICUList currentICUList;
double QoSvalue;
private static List<Double> QoSweight = new ArrayList<>();
private static final int[] clientConstraints_ = new int[]{1,3,5}; //priorities for different metrics, e.g., Trust, Reputation, Reliability
static Matrix comparison;
private static LinkedList<ICU> icuList;
private static final ICUList selectedICUs = new ICUList();
public static ICUListToFromXML ICUInXML_ = new ICUListToFromXML();
private static final File file = new File("ahp_icu.xml");
//private static final File filestart = new File("ahp_icus.xml");


//*** get the current list of ICUs from the ICU Pool ***//

//*** uncomment this method if you have an xml file with ICU profiles and comment the method after this
/*public  static ICUList currentICUsfromXML() throws FileNotFoundException{
     
try {
    currentICUList=ICUListFromXML.getICUsfromXML(file);
            
} 
catch (JAXBException ex) {
    //LoggerMarshallICUListLogger(ICUListToXML_res.class.getName().log(Level.SEVERE, null, ex);
}
return currentICUList;
   
}//end method*/


//use this method if you want to create a new ICU pool and store it in an XML, then get the list of ICUs from XML
public static ICUList currentICUsfromXML() throws FileNotFoundException, JAXBException{
     
ICUList ICUpool = new ICUList();
ICUpool.createHardcodedICUPool();
ICUInXML_.storeICUsInXML(ICUpool, file);
currentICUList=ICUListToFromXML.getICUsFromXML(file);          
return currentICUList;
   
}//end method


public static Matrix createComparisonMatrix(){
    comparison = new Matrix((int) clientConstraints_.length);
    double[] w = new double[clientConstraints_.length];
    int k = 0;
    
    for(int item : clientConstraints_){
       w[k] = item;
        k++;
    }
    for (int i = 0; i <clientConstraints_.length; i++)
        for (int j = i; j < clientConstraints_.length; j++){
        double x = (double) (w[i] / w[j]);
        x = Math.round(x * 100);
        x = x/100;
      int direction = 1;
       if (x <= 1){
           direction = -1;
         x = 1 / x;
        }
        x = ((int)(x / 2)) * 2 + 1;
        x = (double)Math.pow(x, direction);
        comparison.setMatrixValue(i, j, x);
        comparison.setMatrixValue(j, i, 1 / x);
    }
    return comparison;
}//end method    

//***normalize Matrix***//
public static Matrix normalizeMatrix(Matrix comparisonMtrx){
    int rows = comparisonMtrx.rows();
    Matrix normalizedMatrix = new Matrix(rows);
    for (int i = 0; i < comparisonMtrx.rows(); i++){
        for (int j = 0; j < comparisonMtrx.rows(); j++){
            normalizedMatrix.setMatrixValue(i, j, (comparisonMtrx.getValue(i,j)/comparisonMtrx.SumCols(j)));
        //System.out.println(normalizedMatrix.getValue(i, j));
        }
        //System.out.print("\n");
    }
    return normalizedMatrix;
}//end method

public static List<Double> getWeightsVector(){
    Matrix comparisonM = createComparisonMatrix();
    Matrix normalized = new Matrix(comparisonM.rows(), normalizeMatrix(comparisonM).getMatrix());
    int m=0;
    for (int i=0;  i<clientConstraints_.length; i++){
        QoSweight.add((double) (normalized.SumRows(m++)/normalized.columns()));
    }
    return QoSweight;
}//end method


//***compute satisfaction score***//
private static double satisfactionFunction(double offeredValue, double requestedValue, int order){
    double relativeUtility;
    double i;
    relativeUtility = offeredValue / requestedValue;
    if (relativeUtility < 0.5)
        i=0.63;
    else if (relativeUtility < 1)
        i=0.26;
    else if (relativeUtility <= 2)
        i=0.11;
    else i = -1;
    if (i == -1)
    return 0;
    rating = i;
    if (order == -1)
       i = 2 - i;
    //else System.out.println("relative utility value not valid");
    //return ratingVector[i];
    return rating;
}

public static double overallSatisfactionScore(double metric1, double metric2, double metric3){
   
        double s;
        
        offeredValues=new double[]{metric1, metric2,metric3};
        getWeightsVector();
        overallSatisfaction=0;
        double finalSatisfaction=0;
        for (int l=0; l<3; l++){
            QoSr = QoSweight.get(l);
            QoSr = Math.round(QoSr * 100);
            QoSr = QoSr/100;
            System.out.println("Weight");
            System.out.println(QoSr);
            offeredValue_=offeredValues[l];
            //requestedValue_=requestedValues[l];
            requestedValue_= (double) requestedValues.get(l);
       
            s=satisfactionFunction(offeredValue_,requestedValue_,1);
            System.out.println("Satisf.");
    
            System.out.println(s);
            System.out.print("\n");
            overallSatisfaction = overallSatisfaction+(QoSr*s);
            overallSofR.add(overallSatisfaction);
            System.out.println(overallSatisfaction);
            System.out.print("\n");
    }
     //System.out.print("\n");
     
    return overallSatisfaction;
}

public static void main(String[] args) throws FileNotFoundException, JAXBException, IOException, URISyntaxException {
    
    //now find the most appropriate ICU/ICUs for the task
    double a = 0.0,b=0.0,c=0.0; 
    double comparisonS=0;
    currentICUsfromXML();
    double val;
    icuList=currentICUList.getICUPool();
    System.out.println("Set your requirement values for reputaton, socio-tech trust, social trust in that order with double values from 0 to 2");
    cr.setlistreq(0.0, 0.0, 0.0);
     for (int h=0; h<3; h++){
        val=cr.getreqvalues().get(h);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double answer = Double.parseDouble(br.readLine());
        val = answer;
        requestedValues.add(h, answer);
     }
       
       for (int h=0; h<icuList.size(); h++){
        a=icuList.get(h).getICUTrust();
        b=icuList.get(h).getICUReputation();
        c=icuList.get(h).getICUReliability();
        overallSatisfactionScore(a,b,c);
        icuList.get(h).setICUsatisfactionScore(overallSatisfaction);
        overallSperR.add(overallSatisfaction);
     }
    // break;}
    
    double max = Collections.max(overallSperR);
    System.out.println(max);
    
   
    //System.out.println("ICUs ranked by Trust, Reputation and Reliability, with importance scores respectively 1, 3, 5: ");
    
    System.out.println("ICUs with max satisfaction score (ranked with Trust, Reputation and Reliability, with importance scores respectively 1, 3, 5:) ");

    for (int k=0; k<icuList.size(); k++){
    if(icuList.get(k).getICUSatisfactionScore()==max){
      System.out.print(" "+icuList.get(k).getICUId());
      //selectedICUs.add(icuList.get(k));
      
    }
    }   
    System.out.println();
    Collections.sort(icuList, new ICUComp());
    System.out.println("All ICUs ranked with Trust, Reputation and Reliability, with importance scores respectively 1, 3, 5: ");

    for (int k=0; k<icuList.size(); k++){
        System.out.print(" "+icuList.get(k).getICUId());
    }
       
    ICUInXML_.storeICUsInXML((ICUList) icuList, file);  
    
}//end main 

}//end class