/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.slaparameters;

/**
 *
 * @author junnguyen
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        SLAParameterPool pool = new SLAParameterPool();
        
        
        SLAParameter parameter1 = pool.getSLAParameterById(1);
        System.out.println("parameter1: " + parameter1.getParameterName());
        
        
        SLAParameter parameter2 = pool.getSLAParameterByName("willingness");
        System.out.println("parameter2: " + parameter2.getParameterId());
    }
    
}
