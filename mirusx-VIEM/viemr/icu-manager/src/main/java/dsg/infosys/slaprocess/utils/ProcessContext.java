/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.slaprocess.utils;

import java.util.ArrayList;
import java.util.List;
import dsg.infosys.demo_algorithms.SCURun_SLOChanges;

public class ProcessContext {

    private static List<Attribute> attributeList;
    private static SCURun_SLOChanges sCURun_SLOChanges;

    public static void init() {
        attributeList = new ArrayList<>();
    }

    public static void setAttribute(String key, String value) {

        boolean rs = false;

        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                attribute.setValue(value);
                rs = true;
            }

        }

        if (!rs) {
            Attribute attribute = new Attribute(key, value);
            attributeList.add(attribute);
        }

    }

    public static void setAttribute(String key, int value) {

        boolean rs = false;

        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                attribute.setValue(String.valueOf(value));
                rs = true;
            }

        }

        if (!rs) {
            Attribute attribute = new Attribute(key, String.valueOf(value));
            attributeList.add(attribute);
        }

    }

    public static void setAttribute(String key, double value) {

        boolean rs = false;

        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                attribute.setValue(String.valueOf(value));
                rs = true;
            }

        }

        if (!rs) {
            Attribute attribute = new Attribute(key, String.valueOf(value));
            attributeList.add(attribute);
        }

    }

    public static void setAttribute(String key, boolean value) {

        boolean rs = false;

        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                attribute.setValue(String.valueOf(value));
                rs = true;
            }

        }

        if (!rs) {
            Attribute attribute = new Attribute(key, String.valueOf(value));
            attributeList.add(attribute);
        }

    }

    public static String getAttributeStringValue(String key) {
        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                return attribute.getValue();
            }

        }

        return null;
    }

    public static Integer getAttributeIntegerValue(String key) {
        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                return Integer.parseInt(attribute.getValue());
            }

        }

        return null;
    }

    public static Double getAttributeDoubleValue(String key) {
        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                return Double.parseDouble(attribute.getValue());
            }

        }

        return null;
    }

    public static Boolean getAttributeBooleanValue(String key) {
        for (Attribute attribute : attributeList) {
            if (attribute.getKey().equals(key)) {
                return Boolean.parseBoolean(attribute.getValue());
            }

        }

        return null;
    }



    public static SCURun_SLOChanges getsCURun_SLOChanges() {
        return sCURun_SLOChanges;
    }

    public static void setsCURun_SLOChanges(SCURun_SLOChanges sCURun_SLOChanges) {
        ProcessContext.sCURun_SLOChanges = sCURun_SLOChanges;
    }
    
    

}
