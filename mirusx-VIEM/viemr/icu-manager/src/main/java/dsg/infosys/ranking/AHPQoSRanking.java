/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.ranking;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import dsg.infosys.basemodel.ICU;
import dsg.infosys.utils.ICUComp;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.utils.ICUListToFromXML;
import dsg.infosys.basemodel.Matrix;
import static dsg.infosys.demo_algorithms.AHPRankingAlgorithm_Demo.ICUInXML_;
import java.io.File;

/**
 *
 * @author Mirela Riveni
 */
public class AHPQoSRanking {

private static double QoSr;
private static double offeredValue_, requestedValue_;
private static double overallSatisfaction;
private static  List<Double> overallSofR= new ArrayList<>();
private static List<Double> overallSperR= new ArrayList<>();
public static List<Double> sortedScores=new ArrayList<>();
private static double rating;
private static final double[] requestedValues = new double[]{1.5, 2, 0.2};
private static double[] offeredValues = new double[]{};
private static ICUList currentICUList;
double  QoSvalue;
private static List<Double> QoSweight = new ArrayList<>();
private static final int[] clientConstraints_ = new int[]{1,3,5}; //priorities for different metrics, say Trust, Availability, Reputation
static Matrix comparison;
private static ICUList icuList;
public static ICUList sortedICUs;
private static ICUList selectedICUs = new ICUList();
public  static ICU temp;
public static ICU temp1;
public ICUListToFromXML ICUInXML = new ICUListToFromXML();
private static final File file = new File("ahp_icu.xml");


//*** get the current list of ICUs from the ICU Pool ***//


//*** uncomment this method if you have an xml file named 'icus.xml' with ICU profiles
/*public static ICUList currentICUsfromXML() throws FileNotFoundException{
     
try {
    currentICUList=ICUfromXML.getICUsfromXML();       
} 
catch (JAXBException ex) {
    Logger.getLogger(ICUListToFromXML.class.getName()).log(Level.SEVERE, null, ex);
}
    return currentICUList;
   
}//end method*/


//use this method if you want to create a new ICU pool and store it in an XML, then get the list of ICUs from XML
public static ICUList currentICUsfromXML() throws FileNotFoundException, JAXBException{
     
ICUList ICUpool = new ICUList();
ICUpool.createHardcodedICUPool();
ICUInXML_.storeICUsInXML(ICUpool, file);
currentICUList=ICUListToFromXML.getICUsFromXML(file);          
return currentICUList;

}//end method

    

public static Matrix createComparisonMatrix(){
    comparison = new Matrix((int) clientConstraints_.length);
    double[] w = new double[clientConstraints_.length];
    int k = 0;
    
    for(int item : clientConstraints_){
       w[k] = item;
        k++;
    }
    for (int i = 0; i <clientConstraints_.length; i++)
        for (int j = i; j < clientConstraints_.length; j++){
        double x = (double) (w[i] / w[j]);
        x = Math.round(x * 100);
        x = x/100;
      int direction = 1;
       if (x <= 1){
           direction = -1;
         x = 1 / x;
        }
        x = ((int)(x / 2)) * 2 + 1;
        x = (double)Math.pow(x, direction);
        comparison.setMatrixValue(i, j, x);
        comparison.setMatrixValue(j, i, 1 / x);
    }
    return comparison;
}//end method    


//***normalize Matrix***//
public static Matrix normalizeMatrix(Matrix comparisonMtrx){
    int rows = comparisonMtrx.rows();
    Matrix normalizedMatrix = new Matrix(rows);
    for (int i = 0; i < comparisonMtrx.rows(); i++){
        for (int j = 0; j < comparisonMtrx.rows(); j++){
            normalizedMatrix.setMatrixValue(i, j, (comparisonMtrx.getValue(i,j)/comparisonMtrx.SumCols(j)));
        //System.out.println(normalizedMatrix.getValue(i, j));
        }
        //System.out.print("\n");
    }
    return normalizedMatrix;
}//end method


public static List<Double> getWeightsVector(){
    Matrix comparisonM = createComparisonMatrix();
    Matrix normalized = new Matrix(comparisonM.rows(), normalizeMatrix(comparisonM).getMatrix());
    int m=0;
    for (int i=0;  i<clientConstraints_.length; i++){
        QoSweight.add((double) (normalized.SumRows(m++)/normalized.columns()));
    }
    return QoSweight;
}//end method


//***compute satisfaction score***//
private static double satisfactionFunction(double offeredValue, double requestedValue, int order){
    double relativeUtility;
    double i;
    relativeUtility = offeredValue / requestedValue;
    if (relativeUtility < 0.5)
        i=0.63;
    else if (relativeUtility < 1)
        i=0.26;
    else if (relativeUtility <= 2)
        i=0.11;
    else i = -1;
    if (i == -1)
    return 0;
    rating = i;
    if (order == -1)
       i = 2 - i;
    //else System.out.println("relative utility value not valid");
    //return ratingVector[i];
    return rating;
}

public static double overallSatisfactionScore(double metric1, double metric2, double metric3){
   
        double s;

        offeredValues=new double[]{metric1, metric2,metric3};
        getWeightsVector();
        overallSatisfaction=0;
        double finalSatisfaction=0;
        for (int l=0; l<3; l++){
            QoSr = QoSweight.get(l);
            QoSr = Math.round(QoSr * 100);
            QoSr = QoSr/100;
            
            offeredValue_=offeredValues[l];
            requestedValue_=requestedValues[l];
       
            s=satisfactionFunction(offeredValue_,requestedValue_,1);
     
            overallSatisfaction = overallSatisfaction+(QoSr*s);
            overallSofR.add(overallSatisfaction);
         
    }
     //System.out.print("\n");
     
    return overallSatisfaction;
}


public ICUList getRankedICUs() throws FileNotFoundException, JAXBException, IOException, URISyntaxException {

    //now find the most appropriate ICU/ICUs for the task

    double a = 0.0,b=0.0,c=0.0, comparisonS=0.0;
    currentICUsfromXML();
    icuList=currentICUList.getICUPool();
 
    for (int h=0; h<icuList.getICUPool().size(); h++){
     //if (icuList.get(h).getICUId()==0){
        a=icuList.get(h).getICUTrust();
        b=icuList.get(h).getICUReputation();
        c=icuList.get(h).getICUReliability();
        //offeredValues=new double[]{a,b,c};}
        overallSatisfactionScore(a,b,c);
        icuList.get(h).setICUsatisfactionScore(overallSatisfaction);
        overallSperR.add(overallSatisfaction);
     }

    
    double max = Collections.max(overallSperR);
    for (int k=0; k<icuList.size(); k++){
    if(icuList.get(k).getICUSatisfactionScore()==max){
        comparisonS=max;
      selectedICUs.add(icuList.get(k));
    }
    }
   
    Collections.sort(icuList, new ICUComp());
    
    ICUInXML.storeICUsInXML(icuList, file);
    System.out.print("\n");
   
    return icuList;
 
}//end method
}//end class