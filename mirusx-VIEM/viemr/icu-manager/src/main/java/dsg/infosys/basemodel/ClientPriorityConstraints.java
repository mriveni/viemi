                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import java.util.LinkedList;

/**
 *
 * @author Mirela Riveni
 * This class is just to define the client constraints regarding task/ICU profile
 * just for testing
 */
public class ClientPriorityConstraints extends LinkedList{
   
    
    private int trustPriority_;
    private int reputationPriority_;
    private int reliabilityPriority_;
    

  
    public void setTrustPriority(int trustPriority){
      trustPriority_=trustPriority;
    }
    public void setReputationPriority(int reputationPriority){
      reputationPriority_=reputationPriority;
    }
    public void setReliabilityPriority(int reliabilityPriority){
      reliabilityPriority_=reliabilityPriority;
    }
    
     
    public int getTrustPriority(){
        return trustPriority_;
    }
    public int getReputationPriority(){
        return reputationPriority_;
    }
     public int getReliabilityPriority(){
        return reliabilityPriority_;
    }
}//end class
