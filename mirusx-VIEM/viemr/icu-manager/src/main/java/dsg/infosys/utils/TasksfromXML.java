/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.utils;

import dsg.infosys.basemodel.TaskPool;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Mirela Riveni
 */

public class TasksfromXML {
    public static TaskPool getTasksfromXML() throws JAXBException, FileNotFoundException{
    InputStream is = new FileInputStream( "tasks.xml" );
    JAXBContext jaxbContext = JAXBContext.newInstance(TaskPool.class);
    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    
    TaskPool currentTaskPool;
        currentTaskPool = (TaskPool) jaxbUnmarshaller.unmarshal(is);
    return currentTaskPool;
    }
}
