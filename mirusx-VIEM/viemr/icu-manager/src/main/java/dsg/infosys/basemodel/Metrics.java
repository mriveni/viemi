
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import java.util.concurrent.ThreadLocalRandom;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.*;

/**
 *
 * @author Mirela Riveni
 */

@XmlRootElement(name = "metrics")
@XmlAccessorType(XmlAccessType.NONE)
public class Metrics {
    //fields
   
    @XmlAttribute
    private int MetricsListId_;
    
    @XmlElement  (required = true)
    private int ICUid_;
    
    @XmlElement  (required = true)
    private int totalAssignedTasks_;
    
    @XmlElement  (required = true)
    private int totalDelegatedTasks_;
    
    @XmlElement  (required = true)
    private int nonDelegatedTasks_;
    
    @XmlElement  (required = true)
    private int successfulDelegatedTasks_;
    
    @XmlElement  (required = true)
    private int succNonDelTasks_;
   
    @XmlElement  (required = true)
    private double successRate_;
    
    @XmlElement  (required = true)
    private double delegatedSuccessRate_;
    
    @XmlElement  (required = true)
    private double nonDelSuccRate_;
    
    @XmlElement  (required = true)
    private double willingness_;
    
    @XmlElement  (required = true)
    private double willingnessConf_;
    
    @XmlElement  (required = true)
    private double reliability_;
    
    @XmlElement  (required = true)
    private double productivity_;
    
    @XmlElement  (required = true)
    private double MCTS_;
    
    @XmlElement  (required = true)
    private double technicalTrust_;
  
    @XmlElement  (required = true)
    private double socioTechnicalTrust_;
    
 
  
    TaskQueue icuTasks_ = new TaskQueue();
    TaskQueue successfulICUTasks_ = new TaskQueue();
    //TaskQueue failedICUTasks = new TaskQueue(); 
    TaskQueue delegatedTasks_=new TaskQueue();
  
    ICUList icuinvocations_=new ICUList();
 
    private int successfulTasks_;
    private int failedTasks_;
    
    /*@XmlElement(required=true)
    private int sentAcks_;
    
    @XmlElement(required=true)
    private int receivedReqs_;
    
    @XmlElement(required=true)
    //private double reputation_;*/
    
    private int sentAcks_, receivedReqs_, icuinvocationnr_;
   //double wConf=0.0, reliability=0.0, productivity=0.0, reputation;
   double w,pt,mc;
   double random; 
    /*public void setTotalAssignedTasks(double totalAssignedTasks){
        
    }*/
    
  
    //***setter and update methods***
   
   public void setMetricsid(int MetricsListId_){
        this.MetricsListId_=MetricsListId_;
    }
   
   public void setMICUid(int ICUid_){
        this.ICUid_=ICUid_;
    }
   
    public void setTotalAssignedTasks(int totalAssignedTasks_){
        this.totalAssignedTasks_=totalAssignedTasks_;
    }
    

     public void setTotalDelTasks(int totalDelegatedTasks_){
        this.totalDelegatedTasks_= totalDelegatedTasks_;
    }
     
    public void setTotalNonDelTasks(int nonDelegatedTasks_){
        this.nonDelegatedTasks_=nonDelegatedTasks_;
    }
    
   
     public void setSuccDelTasks(int successfullyDelegatedTasks_){
        this.successfulDelegatedTasks_=successfullyDelegatedTasks_;
    }
     
     
    public void setSuccNonDelTasks(int succNonDelTasks_){
        //succNonDelTasks=totalTasks.size()-getSuccessfulDelegatedTasks(totalTasks);
        this.succNonDelTasks_=succNonDelTasks_;
    }
    
    public void setSuccessRate(double successRate_){
        this.successRate_=successRate_;
    }
   
   public void setDelegatedSuccessRate(double delegatedSuccessRate_){
    this.delegatedSuccessRate_=delegatedSuccessRate_;
    }
 
    public void setNonDelSuccessRate(double nonDelSuccRate_){
       
      this.nonDelSuccRate_=nonDelSuccRate_;
    } 
    
     public void setICUAcks(int sentAcks){
        sentAcks_=sentAcks;
    }
    
     public void setICURequests(int requests){
        receivedReqs_=requests;
    }
    public void setWillingness(double willingness_){
        this.willingness_=willingness_;
     }
    
     public void setWillingnessConf(double willingnessConf_){
        this.willingnessConf_=willingnessConf_;
     }
     
    public void setICUInvocations(ICUList icuinvocations_){
        this.icuinvocations_=icuinvocations_;
    }
    
    public void setICUInvocationnr(int icuinvocationnr){
        icuinvocationnr_=icuinvocationnr;
    }
       
     public void setICUReliability(double reliability_){
        this.reliability_=reliability_;
     }

    public void setICUProductivity(double productivity_){
        this.productivity_= productivity_;
    }
     
    public void setMCTS(double MCTS_){
        this.MCTS_=MCTS_;
    }
 
    
    public void setICUPerformanceTrustScore(double technicalTrust_){
        this.technicalTrust_=technicalTrust_;
    }
  
   
    public void setSocioTechnicalTrust(double socioTechnicalTrust_){
        this.socioTechnicalTrust_=socioTechnicalTrust_;
    }
   
     public void setFailedTasks(int failedTasks_){
        this.failedTasks_=failedTasks_;
    }
   
   
         
    //***getter methods***
    
    public int getMetricsid(){
        return MetricsListId_;
    } 
     
    public int getMICUid(){
        return ICUid_;
    } 
     
     
    public int getTotalAssignedTasks(){
        return totalAssignedTasks_;
    }
    
    public TaskQueue getSuccessfulICUTaskQueue(){
        return successfulICUTasks_;
    }
    
    public TaskQueue getAllICUTasks(){
        return icuTasks_;
    }
   
    
    public int getTotalDelTasks(){
      return totalDelegatedTasks_;
    }
    
    
     public int getTotalNonDelTasks(){
        return nonDelegatedTasks_;
    }
     
    public int getSuccessfulTasks(){
        return successfulTasks_;
    }
   
    
    public int getSuccDelTasks(){
        return successfulDelegatedTasks_;
    }
    
    
    public int getSuccNonDelTasks(){
        //succNonDelTasks=totalTasks.size()-getSuccessfulDelegatedTasks(totalTasks);
        return succNonDelTasks_;
    }
    

    public double getICUSuccessRate(){
       //successRate_=icu.getSuccessfulICUTaskQueue().size()/icu.getICUTaskQueue().size();
       return successRate_;
    }
     
    
    public double getDelegatedSuccessRate(){
       //delegatedSuccessRate_=getSuccDelTasks()/getTotalDelTasks();
       return delegatedSuccessRate_;
    }
    
    public double getNonDelSuccessRate(){
        //nonDelSuccRate_=getSuccNonDelTasks()/getTotalNonDelTasks();
        return nonDelSuccRate_;
    }
    
   
     public double getWillingness(){
        return willingness_;
    }
    
    public int getICUAcks(){
        return sentAcks_;
    }
    
    public int getICUReqs(){
        return receivedReqs_;
    }
     public double getICUWillingnessConf(){
        return willingnessConf_;
    }
    
     public double getICUreliability(){
        return reliability_;
    }
    
    public ICUList getICUInvocations(){
        return icuinvocations_;
    }
    
    public int getICUInvocationnr(ICU icu){
        return icuinvocationnr_;
    }
    public double getICUProductivity(){
        return productivity_;
    }
 
        
     public double getMCTS(){
        return MCTS_;
    }
    
     public double getICUPerformanceTrustScore(){
        return technicalTrust_;
    }
    
     public double getICUSocioTechnicalTrust(){
        return socioTechnicalTrust_;
    }
      

     
    //***calculations of aggregated metrics***
    public void calSuccessfulDelNonDelTasks(TaskQueue icuTasks_){
        for (int i=0; i<icuTasks_.size();i++){
          if(icuTasks_.get(i).taskDelegated==true){
                    successfulDelegatedTasks_++;}
                     else //if(totalTasks.get(i).taskDelegated==false)
                     {succNonDelTasks_++;}
            //}
        }
    }
    
    public void calICUwillingnessConf(ICU icu){ 
        //willingnessConf_=(((double)(icu.getAcks()/icu.getReqs()))*((double)getDelegatedSuccessRate()));
        if(getTotalDelTasks()!=0&&getICUReqs()!=0){
    
         //willingnessConf_=(((double)(icu.ICUMetrics_.getICUAcks()/icu.ICUMetrics_.getICUReqs()))*((double)(getSuccDelTasks()/getTotalDelTasks())))/getICUInvocationnr(icu);
         willingnessConf_=(((double)(getICUAcks()/getICUReqs()))*((double)getDelegatedSuccessRate()));}
        else {willingnessConf_=0.1;}
    }
    
    
    public void calICUreliability(){
        if(getICUWillingnessConf()==0.0){
            reliability_= getICUSuccessRate();
        }
        else{
            reliability_=getICUWillingnessConf()*getNonDelSuccessRate();
        }
    }
    
    
    public void calICUInvocations(ICU icu){
        icuinvocations_.add(icu);
        
    }
    
    public void updateInvocationnr(){ 
        icuinvocationnr_++;
    }
    
   
     public void calICUProductivity(TaskQueue icuTasks_){
        productivity_ = ((double)icuTasks_.size()/(double)6);
        if (productivity_>1.0){
            productivity_=0.9;}
        //return productivity;
    }
     
     
     public void updateICUTaskQueue(ICUTask icuTask)
    {
        icuTasks_.add(icuTask);
        successfulICUTasks_.add(icuTask);
        //failedTasks_++;
        //return ICUTasks;
    }

     
    public void updateDelegatedTasks(ICUTask icuTask){ 
        delegatedTasks_.add(icuTask);
        totalDelegatedTasks_++;
    }
    
   
    
   public void calSuccessRate(TaskQueue icuTasks_){
        if(icuTasks_.size()!=0){
             successRate_= (double) successfulICUTasks_.size() / (double)icuTasks_.size();
        }
        else successRate_=0.1;
    }
    
   
   public void calDelNonDelSuccessRate(TaskQueue icuTasks_){
        if(icuTasks_.size()!=0){
            delegatedSuccessRate_= (double) getSuccDelTasks()/(double) getTotalDelTasks() ;
            nonDelSuccRate_= (double) getSuccNonDelTasks()/(double) getTotalNonDelTasks();
        }
    }
    
   
    public void removeTask(ICUTask icuTask){
        successfulICUTasks_.remove(icuTask);
        //return successfulICUTasks;
    }
    
    public void calICUPerformanceTrustScore(){
        if(getICUreliability()!=0.0&&getICUProductivity()!=0.0){
        technicalTrust_=(getICUreliability()*getICUProductivity());}
        else technicalTrust_=0.1;
        //return technicalTrust_;
    }  
    

    
    public void calICUCollaborationScore(){ 
        random=ThreadLocalRandom.current().nextDouble(0.3, 1);
        MCTS_=random;
        
    }
    
    
    public void calICUsocioTechnicalTrust(){
        if (getICUPerformanceTrustScore()!=0.0){
        random=ThreadLocalRandom.current().nextDouble(0, 1);
        socioTechnicalTrust_=(0.6*(double)getICUPerformanceTrustScore())+((0.4)*(double)getMCTS());}
        else socioTechnicalTrust_=0.1;
    }
    
    
    public void updateSentAcks(int sentAcks){
        sentAcks_++;
    }
    public void updateReceivedReqs(int requests){
        receivedReqs_++;
    }
    
    
    public void calNonDelTasks(){
       nonDelegatedTasks_= icuTasks_.size() - getTotalDelTasks();
    }

    public void calFailedTasks(TaskQueue totalTasks){        
        failedTasks_++;
     }
    
     public int getFailedTasks(){
        return failedTasks_;
    }
      
}//end class
