/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import dsg.infosys.agreementmodel.ISLA;
import java.util.HashMap;
import java.util.LinkedList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Mirela Riveni
 */


@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "ICU")
public class ICU{
    
    @XmlAttribute
    private int ICUId_;
    
    private String ICUName_;
    
    @XmlElement
    private String ICUSkill_;
    
    @XmlElement
    private double ICUCostPerTask_;
    
    @XmlElement
    private double ICUTrust_;
    
    @XmlElement
    private double ICUReputation_;
    
    @XmlElement
    private double ICUAvailability_;
    
    @XmlElement
    private double ICUReliability_;
    
    @XmlElement
    private double ICUsatisfactionScore_;
     

   @XmlElement (name = "ICUMetrics_", required = true)
    public Metrics ICUMetrics_=new Metrics();
    ICUList connectedNodes= new ICUList();

    public int failedTasks_;
    private ICUTask taskInQueue;
    private boolean willingness_; 
    private boolean willing;
    public int requests_, requests;
    public int acks_,acks;
    //constant attributes regarding ICU State
    public static final int ICUIDLE = 0; 
    public static final int ICUACTIVE = 1;  
    public static final int ICUSUSPENDED = 2;
    public static final int ICURESUMED = 3;
    public static final int ICUSTOPPED = 4;
    
    private static int ICUState_;
    public  static int ICUCount;
    
    private TaskQueue ICUTasks=new TaskQueue();
    private TaskQueue successfulTasks= new TaskQueue();
    
    private ISLA icuSLA;
    
    public int totaltasks_=0, totaldelt_=0, totalnondelt_=0, sucdelt_=0, sucnondelt_=0;
    public double sucr_=0.0, selsucr_=0.0, nondelsucr_=0.0,  willconf_=0.0, reliab_=0.0, prod_=0.0, pt_=0.0, mcts_=0.0, socioTechnicalTrust_=0.0;
    boolean will_;
     
    public int scuInvocation=0;
  
    HashMap hashicumetrics=new HashMap();  
    
    
    //constructors
     public ICU(){
    }
 
    
    public ICU(int ICUId){
      ICUId_=ICUId;
      ICUCount++;
      ICUName_=null;
    }
    
    public ICU(int ICUId, String ICUName){
      ICUId_=ICUId;
      ICUName_=ICUName;
      ICUCount++;
    }
     
    //***setter methods***
    public void setICUId(int ICUId){
        ICUId_=ICUId;
    }
   
    public void setICUSkill(String ICUSkill){
        ICUSkill_=ICUSkill;
    }
    
    public void setICUCostPerTask(double ICUCostPerTask){
        ICUCostPerTask_=ICUCostPerTask;
     }
    
    public void setICUTrust(double ICUTrust){
        ICUTrust_=ICUTrust;
    }

     public void setConnectedNodes(ICU nodeicu){
        connectedNodes.add(nodeicu);
        
     }
     
     public ICUList getConnectedNodes(){
         return connectedNodes;
     }
        
    public void setICUReputation(double ICUReputation){
        ICUReputation_=ICUReputation;
    }
    
    public void setICUAvailability(double ICUAvailability){
        ICUAvailability_=ICUAvailability;
    }
    
    public void setICUReliability(double ICUReliability){
        ICUReliability_=ICUReliability;
    }
    
    public void setICUState(int ICUState){
        ICUState_=ICUState;
    }
    
     public void setICUAcks(int acks){
        acks_=acks;
    }
    
     public void setICURequests(int requests){
        requests_=requests;
    }
    public void setICUMetrics(Metrics ICUMetrics_){
        this.ICUMetrics_=ICUMetrics_;
    }
    
    
    public void setICUsatisfactionScore(double ICUsatisfactionScore){
        ICUsatisfactionScore_=ICUsatisfactionScore;
    }
   
    public void updateICUTaskQueue(ICUTask icuTask){
        ICUTasks.add(icuTask);
        successfulTasks.add(icuTask);
        failedTasks_++;
        //return ICUTasks;
    }

     /*public void updateICUAcks(){
       acks++;
    }
     
      public void updateICURequests(){
       requests++;
    }*/
    
    public TaskQueue removeTask(ICUTask icuTask){
        successfulTasks.remove(icuTask);
        return successfulTasks;
    }
    
    public void setWillingness(boolean willingness){
        willingness_=willingness;
    }
    
    
    //***getter methods*** 
    public int getICUId(){
        return ICUId_;
    }
    
    public String getICUName(){
        return ICUName_;
    }
    
    public String getICUSkill(){
        return ICUSkill_;
    }
    
    public double getICUCostPerTask(){
        return ICUCostPerTask_;
    }
    
    public double getICUTrust(){
        return ICUTrust_;
    }
    
    public double getICUReputation(){
        return ICUReputation_;
    }
    
    public double getICUAvailability(){
        return ICUAvailability_;
    }
    
    public double getICUReliability(){
        return ICUReliability_;
    }
    
    public int getICUState(){
        return ICUState_;
    }
    
    public double getICUSatisfactionScore(){
        return ICUsatisfactionScore_;
    }
    
    public TaskQueue getICUTaskQueue(){
        return ICUTasks;
    }
    public TaskQueue getSuccessfulICUTaskQueue(){
        return successfulTasks;
    }
    
    
    public boolean getWillingness() {
      
        willing=true;
        return willing;
    }
    
    public int getReqs() {
        return requests;
    }
    
    public int getAcks() {
        return acks;
    }
    
    public Metrics getICUMetrics(){
       return ICUMetrics_;
    }
    
    // ***Get the state of an ICU in a String type.***
    public static String getICUStateString(int ICUState){
        String ICUStateString = null;
        switch(ICUState){
            case ICUIDLE:
                 ICUStateString="ICUIdle";
                 break;
            case ICUACTIVE:
                 ICUStateString="ICUActive";
                break;
            case ICUSUSPENDED:
                 ICUStateString="ICUSuspended";
                 break;
            case ICURESUMED:
                 ICUStateString="ICUResumed";
                 break;
            case ICUSTOPPED:
                 ICUStateString="ICUStopped";
                 break;
            default:
                 break;
        }
        return ICUStateString;
    }
       
          
   
}
