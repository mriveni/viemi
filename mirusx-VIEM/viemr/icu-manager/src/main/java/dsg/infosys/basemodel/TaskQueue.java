/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Mirela Riveni
 */
public class TaskQueue extends LinkedList<ICUTask>{
    
public List<ICUTask> suspendedList;
public List<ICUTask> finishedList;
public int finishedCounter;
  
//method to return a Task with the specified id
    public ICUTask getTaskwithId(int id){
        ICUTask requestedTask=null;
        Iterator<ICUTask> find = super.iterator();
        while (find.hasNext()){
            requestedTask=find.next();
            if (requestedTask.getTaskId()==id){
                return requestedTask;
            }
        }
        return null;
    }//end method
    
    
    
 //
    public int setTaskinListState(int TaskinListState, int ICUId){
        ICUTask requestedTask=null;
        Iterator<ICUTask> find = super.iterator();
        while (find.hasNext()){
            requestedTask= find.next();
            if (requestedTask.getTaskId()== ICUId){
                requestedTask.setTaskState(TaskinListState);
            }
        }
        return TaskinListState;
 }//end method
    
    
//method to get the list of Tasks that are suspended
    public List<ICUTask> getNrSuspendedTasks(){
        int suspendedCounter = 0;
        ICUTask suspendedTask = null;

        // a loop that counts the number of suspended Tasks
        Iterator<ICUTask> it = super.iterator();
        while (it.hasNext()){
            suspendedTask = it.next();
            if (suspendedTask.getTaskState() == suspendedTask.TASKSUSPENDED) {
                suspendedCounter++;
                suspendedList.add(suspendedTask);
            }
        }
        return suspendedList;
    }   
 
 //method to get the list of Tasks that are finished   
    public List<ICUTask> getNrFinishedTasks(){
        finishedCounter = 0;
        ICUTask finishedTask = null;

        // a loop that counts the number of suspended Tasks
        Iterator<ICUTask> it = super.iterator();
        while (it.hasNext()){
            finishedTask = it.next();
            if (finishedTask.getTaskState() == finishedTask.TASKFINISHED) {
                finishedCounter++;
                suspendedList.add(finishedTask);
            }
        }
        return finishedList;
    }  
  
    
 //method to get the Task in a specific position in the list   
    public ICUTask getTaskInPos(int index){
        ICUTask posTask = null;
        Iterator<ICUTask> it = super.iterator();
        int count = 0;
        while ( (it.hasNext()) && (count <= index) )
        {
            count++;
            posTask = it.next();
        }

        return posTask;
    }
}//end class
