/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.utils;

import dsg.infosys.basemodel.MetricsList;
import dsg.infosys.ulys.resource.MetricPool;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author Mirela Riveni
 */
public class ICUMetricsToXML{

    
   public static void metricsToXML() throws IOException, FileNotFoundException, URISyntaxException, Exception{
    
     
      MetricsList metricsm = MetricPool.initMetricsList();
      
  
      try {
            
            File file = new File("icus_trustttt.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(MetricsList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //print the XML output
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(metricsm, file);
            jaxbMarshaller.marshal(metricsm, System.out);
        }
        
        catch (JAXBException e) {
	}
        
   
    
}
}

