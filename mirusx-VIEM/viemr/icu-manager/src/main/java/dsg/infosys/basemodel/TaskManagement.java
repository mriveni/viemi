/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import java.util.LinkedList;

/**
 *
 * @author Mirela Riveni
 */

public interface TaskManagement {
     
    /***
     * 
     * @param TaskId of the Task belonging to a specific SCU.
     * @param SCUId of the SCU that the ICU belongs to.
     * When implementing the method set the state of the ICU to ACTIVE.
     */
    public void addTask(ICUTask TaskId, SCU SCUId);
    
    
    /***
     * 
     * @param TaskId of the Task to be suspended, depending on the scheduler.
     * @param SCUId of the SCU that the ICU belongs to.
     * When implementing the method set the state of the ICUTask to SUSPENDED. 
     */
    public void suspendTask(ICUTask TaskId, SCU SCUId); 
    
    
    /***
     * 
     * @param TaskId of the Task in IDLE or SUSPENDED state that gets tobe resumed.
     * @param SCUId of the SCU that the ICU belongs to.
     * When implementing the method set the ICUTask state to RESUMED and after a time period to ACTIVE.
     */
    public void resumeTask(ICUTask TaskId, SCU SCUId);
    
    
    /***
     * 
     * @param TaskId of the Task that is finished, according to a scheduler.
     * @param SCUId of the SCU that the task belongs to.
     * When implementing the method set the ICUTask state to FINISHED.
     */
    public void finishTask(ICUTask TaskId, SCU SCUId);
    
    
    /***
     * 
     * @param TaskId of the Task that is assigned to the returned ICU.
     * @return the ICU to which the Task with TaskId is assigned.
     */
    public ICU getCurrentAssignedICU(ICUTask TaskId);
    
     
    /***
     * 
     * @param TaskId of the Task to be delegated.
     * @param ICUId_1 
     * @param ICUId_2
     * @return 
     */
    public LinkedList<ICUTask> delegateTask(ICUTask TaskId, ICU ICUId_1, ICU ICUId_2);
   
    /***
     * 
     * @param SCUId of the SCU for which we request the Tasks belonging to it.
     * @return a list of Tasks belonging to the specific SCU.
     */
    public LinkedList<ICUTask> getAllTasksInSCU(SCU SCUId);

}
