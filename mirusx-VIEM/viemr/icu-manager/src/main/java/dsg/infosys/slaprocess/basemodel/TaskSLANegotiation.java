/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.slaprocess.basemodel;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.common.entity.eda.elasticprocess.Task;
import dsg.infosys.demo_algorithms.SCURun_SLOChanges;
import dsg.infosys.slaprocess.utils.ProcessContext;


public class TaskSLANegotiation implements Task{

    @Override
    public void execute() {
        try {
            System.out.println("Task: SLAs Negotiation");
            SCURun_SLOChanges sttalgo = ProcessContext.getsCURun_SLOChanges();
            
            ICUList listOfSelectedICUs = sttalgo.generateICUs();
            sttalgo.clearICUTrustScores(listOfSelectedICUs);

            sttalgo.algo(listOfSelectedICUs, true);
        } catch (Exception ex) {
            Logger.getLogger(TaskSLANegotiation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ProcessContext.setAttribute("GatewayDirection", 2);
    }

}
