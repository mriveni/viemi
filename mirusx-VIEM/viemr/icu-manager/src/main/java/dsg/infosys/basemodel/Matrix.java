/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

/**
 *
 * @author Mirela Riveni
 * This class is used in the AHP algorithm for ranking resources.
 */

public class Matrix {
    
    private int row, col; //number of rows and columns
    private double[][] matrix; //a 'row by column' array
    
    public Matrix(int m, int n){
        this.row = m;
        this.col = n;
        matrix = new double[m][n];
    }
    
    public Matrix(int row, double[][] value){
        this.row = row;
        this.col = row;
        matrix = new double[row][row];
        matrix = value;
    }
    
    public Matrix(int m){
        this.row = m;
        this.col = m;
        this.matrix = new double[m][m];
        for(int i = 0; i < m; i++)
        for (int j = 0; j < m; j++)
        {
         matrix[i][j] = -1;
        }
    }

    public Matrix()
    {
        
    }
    
    public int columns()
    {
        return col;
    }
    
    public int rows()
    {
        return row;
    }
    
    //setter methods
    // set Matrix values 
    public void setMatrixValue(int i, int j, double value)
    {
        matrix[i][j] = value;
    }
    
     public void setMatrix(double[][] value){
        for (int i = 0; i < row; i++)
        for (int j = 0; j < col; j++)
        matrix[i][j] = value[i][j];
    }
   
    //getter methods
    //get Matrix values
    public double[][] getMatrix(){
        return this.matrix;
    }
    
    public double getValue(int i, int j){
        return matrix[i][j];
    }
    
  
    // compute power of 2 of a matrix 
    public double[][] Power(){
        if (this.row != this.col)
            return null;
        double[][] matrix2;
        matrix2 = new double[row][col];
        for (int i = 0; i < row; i++)
        for (int j = 0; j < row; j++){
                matrix2[i][j] = 0;
                for (int k = 0; k < row; k++)
                    matrix2[i][j] += matrix[i][k] * matrix[k][j];
            }
        return matrix2;
    }
   
    //methods to compute the sum of the columns of the matrix
    public float[] SumCols()
    {
        float[] sum = new float[col];
        for (int i = 0; i < col; i++)
        {
            sum[i] = 0;
            for (int j = 0; j < row; j++)
                sum[i] += matrix[j][i];
        }
        return sum;
    }
    
    public float SumCols(int j)
    {
        float sum =0;
        for (int i = 0; i < col; i++)
              sum += matrix[i][j];
        return sum;
    }
    
    //methods to compute the sum of the rows of the matrix
    public float[] SumRows()
    {
        float[] sum = new float[row];
        for (int i = 0; i < row; i++) {
            sum[i] = 0;
            for (int j = 0; j < col; j++)
            sum[i] += matrix[i][j];
        }
        return sum;
    }
    
    public float SumRows(int i)
    {
        float sum = 0;
        for (int j = 0; j < col; j++){
            sum += matrix[i][j];
        }
        return sum;
    }
    
    //sort
    public void SortColumns(int column, int order)
    {
    //Order =  1 : Accending sort
    //        -1 : Descending sort
    double[] tmp = new double[col];
    for(int i=0;i<row-1;i++)
    for (int j = i + 1; j < row; j++){
        if (matrix[i][column]*order < matrix[j][column]*order)
            for (int k = 0; k < col; k++){
                tmp[k] = matrix[i][k];
                matrix[i][k] = matrix[j][k];
                matrix[j][k] = tmp[k];
            }
    }

    }//end sort

}
