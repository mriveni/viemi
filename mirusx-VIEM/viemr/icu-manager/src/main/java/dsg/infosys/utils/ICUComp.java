/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.utils;

import dsg.infosys.basemodel.ICU;
import java.util.Comparator;


public class ICUComp implements Comparator<ICU>{

    Double smth;
    Double smthsmth;

    @Override
    public int compare(ICU icu1, ICU icu2) {
       
        smth= Double.valueOf(icu1.getICUSatisfactionScore());
        smthsmth = Double.valueOf(icu2.getICUSatisfactionScore());
        if (smth.compareTo(smthsmth) > 0) {
            return -1;
        } else if (smth.compareTo(smthsmth) < 0) {
            return 1;
        } else {
            return 0;
        
    }
}
    
}
