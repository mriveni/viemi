/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.utils;

import dsg.infosys.basemodel.ICUList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Mirela Riveni
 */
public class ICUListToFromXML {

    
    //private static final String ICU_XML_FILE="icu.xml";
    //private static final String TASK_XML_FILE="tasks.xml";
    public void storeICUsInXML(ICUList ICUpool, File file) throws FileNotFoundException{
      
       try {
            
            //File file = new File("icus.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(ICUList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //print the XML output
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(ICUpool, file);
        }
        
        catch (JAXBException e) {
	}
        
    }
    
    
     public static ICUList getICUsFromXML(File fileFrom) throws JAXBException, FileNotFoundException{
        InputStream is = new FileInputStream(fileFrom);
        JAXBContext jaxbContext = JAXBContext.newInstance(ICUList.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    
        ICUList currentICUPool;
        currentICUPool = (ICUList) jaxbUnmarshaller.unmarshal(is);

        return currentICUPool;
    }

}
