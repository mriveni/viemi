/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirela Riveni
 * Task Pool Generator Class
 * 
 */
@XmlRootElement(name = "Tasks")
@XmlAccessorType(XmlAccessType.FIELD)
public class TaskPool {
   
    @XmlElement(name = "Task")
    private TaskQueue taskPool;
    
    int id=0;
    int tempProp,tempDeadline,tempTime;
    ICUPropList propList = new ICUPropList();
    String skill;
    double cost;
    double trust;
    double reputation;
    double availability;
    double reliability;
    double satisfactionScore;
    int deadline;
 
   
    public TaskPool(){
        this.taskPool = new TaskQueue();
    }
    
    //hardcoded version of generating 50 Tasks
    //ToDo: XML data and REST
    public void createHardcodedTaskPool(){
        for(int i=0; i<50; i++){
            ICUTask temp=new ICUTask();
            temp.setTaskId(i);
            tempProp = (int)(Math.random()*5);
            tempDeadline = (int)(Math.random()*6);
            skill = propList.ICUskill[tempProp];
            cost = propList.ICUcostPerTask[tempProp];  
            deadline = temp.timeUnits[tempDeadline];
            tempTime= (int)(Math.random()*4);
      
            temp.setSkillReq(skill);
            temp.setTaskPrice(cost);
            temp.setTaskDeadline(deadline);
            temp.setTaskTime(tempTime);
        
            taskPool.add(temp);  
        }
    }
    
    //setter method
    public void setTasks(TaskQueue taskPool){
    this.taskPool=taskPool;
    }
    
    //returns the list of existing ICUTask objects in the Task Pool
    public TaskQueue getTaskPool(){
        return taskPool;
    }
    
    
  public void printTaskinList(){
    ICUTask requestedTask;
    for (int j=0;j<taskPool.size();j++){
         requestedTask = taskPool.get(j);
         System.out.println("requestedICU properties: "+"skill: "+requestedTask.getSkillReq()+" "+"price: "+ requestedTask.getTaskPrice()+" "+"icuid "+taskPool.get(j).getTaskId()+" "+ taskPool.get(j).getTaskOwner().getICUSkill());
     }
  } 
    
}//end class
