/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.agreementmodel;

import dsg.infosys.slaparameters.SLAParameter;
import java.util.ArrayList;

/**
 *
 * @author Mirela Riveni
 */

//SLAs for Social Compute Units (SCUs).
public class SSLA implements HSLA{
    
   int sSLAId;
   
   private SLAParameter sslaParameters;
   private ArrayList<Integer> intsParties;
   
   private String serviceProvider, serviceConsumer;
   
   public static final int ICUMANAGER = 0; 
   public static final int CUSTOMER = 1; 
     
   //constructor
   public SSLA(int sSLAId, SLAParameter sslaParameters){
        this.sslaParameters=sslaParameters;
        this.sSLAId=sSLAId;
        intsParties.add(ICUMANAGER);
        intsParties.add(CUSTOMER);
    }


    @Override
    public SLAParameter getSLAParameters() {
        return sslaParameters;
    }

    @Override
    public ArrayList<Integer> getSLAParties() {
          return intsParties;
    }
    
    
    public static String slaPartiesString(int sslaParty){
        String sslaPartyString = null;
        switch(sslaParty){
            case ICUMANAGER:
                 sslaPartyString="ICUManager";
                 break;
            case CUSTOMER:
                 sslaPartyString="Customer";
                break;
            default:
                 break;
        }
        return sslaPartyString;
    }
     
}
