/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;
import java.lang.Math;
import java.util.LinkedList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirela Riveni
 * ICU Pool Generator Class
 * 
 */

//@XmlRootElement(name = "ICUs")
//@XmlAccessorType(XmlAccessType.FIELD)
public class ICUPool{
    
    //@XmlElement(name = "ICU")
    private ICUList ICUpool;
    
    
    int id=0;
    int tempProp;
    ICUPropList propList = new ICUPropList();
    String skill;
    double cost;
    double trust;
    double reputation;
    double availability;
    double reliability;
    double satisfactionScore;
    private Metrics icumetrics;
 
   
    public ICUPool(){
        this.ICUpool = new ICUList();
    }
    
 
    public void createHardcodedICUPool(){
        for(int i=0; i<200; i++){
            ICU temp=new ICU();
            temp.setICUId(i);
            tempProp = (int)(Math.random()*5);
            skill = propList.ICUskill[tempProp];
            cost = propList.ICUcostPerTask[tempProp];  
            trust = propList.ICUtrust[tempProp];  
            reputation = propList.ICUreputation[tempProp];
            availability = propList.ICUavailability[tempProp];  
            reliability = propList.ICUreliability[tempProp];
           
            
            temp.setICUSkill(skill);
            temp.setICUCostPerTask(cost);
            temp.setICUTrust(trust);
            temp.setICUReputation(reputation);
            temp.setICUAvailability(availability);
            temp.setICUReliability(reliability);
            temp.setWillingness(true);
            
            //System.out.println(ICU.ICUCount);
            ICUpool.add(temp);  
        }
    }
    
    //setter method
    public void setICUs(ICUList ICUpool){
    this.ICUpool=ICUpool;
    }
    
    //returns the list of existing ICU objects in the ICU Pool
    public ICUList getICUPool(){
        return ICUpool;
    }
    

    
  public void printICUinList(){
    ICU requestedICU;
    for (int j=0;j<ICUpool.size();j++){
         requestedICU = ICUpool.get(j);
         System.out.println("requestedICU properties: "+"skill: "+requestedICU.getICUSkill()+" "+"price: "+ requestedICU.getICUCostPerTask()+" "+"icuid "+ICUpool.get(j).getICUId()+" "+ ICUpool.getICUwithId(j).getICUSkill());
     }
  } 
  

}//end class
