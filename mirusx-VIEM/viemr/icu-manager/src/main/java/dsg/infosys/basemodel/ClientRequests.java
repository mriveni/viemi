/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import java.util.HashMap;
import java.util.LinkedList;


/**
 *
 * @author Mirela Riveni
 */
public class ClientRequests {
    
    Metrics toClient = new Metrics();
    LinkedList <Double> metrics = new LinkedList();
    private double reqTrust=0.0, reqRep=0.0, reqRel=0.0;
    public static HashMap<String, Double> metricRequirements;

    public void setrelreq(double reqTrust){
    reqTrust=this.reqTrust;
    }    
    
    public void settrustreq(double reqRep){
    reqRep=this.reqRep;
    }
   
    public void setmctsreq(double reqRel){
    reqRel=this.reqRel;
    }
    
    public void setlistreq(double reqTrust, double reqRep, double reqRel){
        metrics.add(reqTrust);
        metrics.add(reqRep);
        metrics.add(reqRel);
    }
    
    public LinkedList<Double> getreqvalues(){       
        return metrics;
    }
 
}
