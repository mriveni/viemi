/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsg.infosys.basemodel;

import java.util.Iterator;
import java.util.LinkedList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirela Riveni This class extends the LinkedList<ICU> with some
 * additional methods to better manage ICUs We use this class to create the ICU
 * Pool in the ICUPool Class
 */
@XmlRootElement(name = "ICUs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ICUList extends LinkedList<ICU> {

    @XmlElement(name = "ICU")
    private static ICUList ICUpool = new ICUList();

    int id = 0;
    int tempProp, tempPropc;
    ICUPropList propList = new ICUPropList();
    String skill;

    double cost;
    double trust;
    double reputation;
    double availability;
    double reliability;
    double satisfactionScore;

    //@XmlElement(name = "metrics")
    Metrics metrics;

    public void createHardcodedICUPool() {
        for (int i = 0; i < 200; i++) {
            ICU temp = new ICU();
            temp.setICUId(i);
            temp.ICUMetrics_ = new Metrics();
            metrics = temp.ICUMetrics_;
            metrics.setMetricsid(i);
            tempProp = (int) (Math.random() * 5);
            tempPropc = (int) (Math.random() * 5);
            skill = propList.ICUskill[tempProp];
            cost = propList.ICUcostPerTask[tempPropc];
            trust = propList.ICUtrust[tempProp];
            reputation = propList.ICUreputation[tempProp];
            availability = propList.ICUavailability[tempProp];

            temp.setICUSkill(skill);
            temp.setICUCostPerTask(cost);
            temp.setICUTrust(trust);
            temp.setICUReputation(reputation);
            temp.setICUAvailability(availability);
            temp.setICUReliability(reliability);
            temp.setWillingness(true);

            ICUpool.add(temp);
        }
    }

    //setter method
    public void setICUs(ICUList ICUpool) {
        this.ICUpool = ICUpool;
    }

    //returns the list of existing ICU objects in the ICU Pool
    public ICUList getICUPool() {
        return ICUpool;
    }

    public void printICUinList() {
        ICU requestedICU;
        for (int j = 0; j < ICUpool.size(); j++) {
            requestedICU = ICUpool.get(j);
            System.out.println("requestedICU properties: " + "skill: " + requestedICU.getICUSkill() + " " + "price: " + requestedICU.getICUCostPerTask() + " " + "icuid " + ICUpool.get(j).getICUId() + " " + ICUpool.getICUwithId(j).getICUSkill());
        }
    }

//method to return an ICU with the specified ICUId
    public ICU getICUwithId(int id) {
        ICU requestedICU = null;
        Iterator<ICU> find = super.iterator();
        while (find.hasNext()) {
            requestedICU = find.next();
            if (requestedICU.getICUId() == id) {
                return requestedICU;
            }
        }
        return null;
    }//end method

//method to set the state of an ICU in the list of ICUs
    public int setICUinListState(int ICUinListState, int ICUId) {
        ICU requestedICU = null;
        Iterator<ICU> find = super.iterator();
        while (find.hasNext()) {
            requestedICU = find.next();
            if (requestedICU.getICUId() == ICUId) {
                requestedICU.setICUState(ICUinListState);
            }
        }
        return ICUinListState;
    }

//method to get the ICU in a specific position in the list   
    public ICU getICUInPos(int index) {
        ICU posICU = null;
        Iterator<ICU> it = super.iterator();
        int count = 0;
        while ((it.hasNext()) && (count <= index)) {
            count++;
            posICU = it.next();
        }
        return posICU;
    }

}//end class
