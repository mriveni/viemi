/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.basemodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mirela Riveni
 */

@XmlRootElement(name = "Task")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ICUTask {
  
@XmlAttribute
private int taskId_;

@XmlElement
private String skillReq_;

@XmlElement
private double price_;

@XmlElement
private int taskDeadline_;

private ICU taskOwner_;
private int taskState_, taskTime_;
public  static int taskCount; 


int[] timeUnits = {10,20,40,60,120,180,240};
private int taskInQueue_;
public boolean taskDelegated=false;

//constant attributes regarding Task State
public static final int TASKIDLE = 5; 
public static final int TASKACTIVE = 6;  
public static final int TASKSUSPENDED = 7;
public static final int TASKRESUMED = 8;
public static final int TASKFINISHED = 9;

 //constructors
    public ICUTask(){
   
    }
    
    public ICUTask(int taskId){
      taskId_=taskId;
      taskCount++;
    }
    
   
//setter methods
public void setTaskId(int taskId){
    taskId_=taskId;
}
public void setSkillReq(String skillReq){
    skillReq_=skillReq;
}
public void setTaskPrice(double price){
    price_=price;
}
public void setTaskOwner(ICU taskOwner){
    taskOwner_=taskOwner;
}
public void setTaskState(int taskState){
    taskState_=taskState;
}
public void setTaskDeadline(int taskDeadline){
    taskDeadline_=taskDeadline;
}

public void setTaskInQueue(int taskInQueue){
    taskInQueue_=taskInQueue;
}

public void setTaskTime(int taskTime){
    taskTime_=taskTime;
}

//getter methods
public int getTaskId(){
    return taskId_;
}
public String getSkillReq(){
    return skillReq_;
}

public double getTaskPrice(){
    return price_;
}

public ICU getTaskOwner(){
    return taskOwner_;
}

public int getTaskState(){
    return taskState_;
}

public int getTaskTime(){
    return taskTime_;
}

public int getTaskDeadline(){
    return taskDeadline_;
}
public int getTaskInQueue(){
    return taskInQueue_;
}
    
// ***get the state of an ICU in a String type***
public static String getTaskStateString(int TaskState){
    String TaskStateString = null;
    switch(TaskState){
        case TASKIDLE:
            TaskStateString="TASKIdle";
            break;
        case TASKACTIVE:
            TaskStateString="TASKActive";
            break;
        case TASKSUSPENDED:
            TaskStateString="TASKSuspended";
            break;
        case TASKRESUMED:
            TaskStateString="TASKResumed";
            break;
        case TASKFINISHED:
            TaskStateString="TASKFinished";
            break;
        default:
            break;
    }
    return TaskStateString;
}
}//end class
