/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.slaprocess.utils;

import java.util.List;
import dsg.infosys.common.entity.eda.elasticprocess.Action;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.eda.elasticprocess.Task;
import dsg.infosys.common.entity.runtime.ExecutionStep;
import dsg.infosys.slaprocess.basemodel.TaskFindingNewICU;
import dsg.infosys.slaprocess.basemodel.TaskICUAssignment;
import dsg.infosys.slaprocess.basemodel.TaskSLANegotiation;
import dsg.infosys.slaprocess.basemodel.TaskUpdateSLAs;
import dsg.infosys.slaprocess.basemodel.TaskWorkingWithoutChangesICU;
import dsg.infosys.process.engine.dataelasticitycontroller.DEPExecutionPlanning;


public class SLAProcessEngine {
    
    private SLAChangeProcess slaChangeProcessModel;
  
    public SLAProcessEngine(SLAChangeProcess slaChangeProcessModel) {
        this.slaChangeProcessModel = slaChangeProcessModel;
        
    }
    
    public void execute(){
        DEPExecutionPlanning depep = new DEPExecutionPlanning(slaChangeProcessModel);
        List<ExecutionStep> executionPlan = depep.planningExecution();
        
        for (ExecutionStep executionStep : executionPlan) {

            List<String> prerequisiteActions = executionStep.getListOfPrerequisiteActions();
            List<String> executionActions = executionStep.getListOfExecutionActions();

            System.out.println("--- ExecutionStep "+ executionPlan.indexOf(executionStep)+" ---");

            for (String actionID : prerequisiteActions) {
                System.out.println("Prereuqisite Action: " + actionID);
            }

            for (String actionID : executionActions) {
                System.out.println("Execution Action: " + actionID);
                int index = findIndexOfTask(actionID);
               // System.out.println("index: " + index);
                Task task = slaChangeProcessModel.getListOfTasks().get(index);
                runTask(task);
                
                
            }

            System.out.println("\n");

        }
    }
    
    private void runTask(Task task){
        
        if (task instanceof TaskUpdateSLAs) {
            
            TaskUpdateSLAs t = (TaskUpdateSLAs)task;
            t.execute();
            
            
        } else if (task instanceof TaskSLANegotiation) {
            
            TaskSLANegotiation t = (TaskSLANegotiation)task;
            t.execute();
            
        } else if (task instanceof TaskWorkingWithoutChangesICU) {
            
            TaskWorkingWithoutChangesICU t = (TaskWorkingWithoutChangesICU)task;
            t.execute();
            
        } else if (task instanceof TaskFindingNewICU) {
            
            TaskFindingNewICU t = (TaskFindingNewICU)task;
            t.execute();
            
        } else if (task instanceof TaskICUAssignment) {
            
            
            TaskICUAssignment t = (TaskICUAssignment)task;
            t.execute();
        }
        
        
    }
    
    public int findIndexOfTask(String taskId){
        int index = -1;
        
        List<Action> listOfActions = slaChangeProcessModel.getDirectedAcyclicalGraph().getListOfActions();
        
        for (int i=0;i<listOfActions.size();i++){
            Action action = listOfActions.get(i);
            if (action.getActionID().equals(taskId)) {
                index = i;
            }
        }
        
        return index;
        
    }
    
    
    
}
