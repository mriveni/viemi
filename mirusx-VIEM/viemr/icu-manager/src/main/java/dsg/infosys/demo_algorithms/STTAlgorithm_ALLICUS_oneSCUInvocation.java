/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dsg.infosys.demo_algorithms;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import dsg.infosys.ranking.AHPQoSRanking;
import dsg.infosys.basemodel.ICU;
import dsg.infosys.basemodel.ICUList;
import dsg.infosys.utils.ICUListToFromXML;
import dsg.infosys.basemodel.ICUTask;
import dsg.infosys.basemodel.Metrics;
import dsg.infosys.basemodel.MetricsList;
import dsg.infosys.basemodel.SCU;
import dsg.infosys.basemodel.TaskPool;
import dsg.infosys.basemodel.TaskQueue;
import dsg.infosys.utils.TasksfromXML;



/**
 *
 * @author Mirela Riveni
 */
public class STTAlgorithm_ALLICUS_oneSCUInvocation {
private static ICU icu, icutemp, currentICU;    
private static ICUTask task, tempTask;
public static ICUList icus = new ICUList();   
public static ICUList tempicus = new ICUList(); 
public static TaskQueue taskList, allicutasks, dynamicicutasklist, currentICUtaskList;
public static TaskPool taskPool;
static AHPQoSRanking selectedicus;
private static SCU currentSCU;
public static ICUList icuPool;

public static int[] minutes = new int[300]; 
private static int queueTime, tempTime;
private static ICU tempICU;
private static Metrics metrics;

public MetricsList metricsList;
public static final Map<ICU, Metrics> myMap = new HashMap<>();
public int totaltasks=0, totaldelt=0, totalnondelt=0, sucdelt=0, sucnondelt=0, totaltemp;
double sucr=0.0, delsucr=0.0, nondelsucr=0.0,  willconf=0.0, reliab=0.0, prod=0.0, pt=0.0, mcts=0.0, socioTechnicalTrust=0.0;
boolean will;
DecimalFormat df = new DecimalFormat("#.0000"); 
//public static TaskPool currentTasksfromXML() throws FileNotFoundException{



public static TaskPool generateTasks_(){
    
    taskPool=new TaskPool();
    taskPool.createHardcodedTaskPool();
    
    return taskPool;
}

    public STTAlgorithm_ALLICUS_oneSCUInvocation() {
        this.metricsList = new MetricsList();
    }//
    


 public ICUList algo() throws FileNotFoundException, JAXBException, IOException, URISyntaxException{
    selectedicus = new AHPQoSRanking();
    generateTasks_();
    icus=selectedicus.getRankedICUs();
    icus.setICUs(new ICUList());
    icus.setICUs(selectedicus.getRankedICUs());
   
    taskList=taskPool.getTaskPool();
    currentSCU = new SCU();
    task = new ICUTask();
    tempTask=new ICUTask();
    icu=new ICU();
    icutemp=new ICU();
    currentICU= new ICU();
    currentICUtaskList=currentICU.getICUTaskQueue();
    int min=0;
    for (int i=0; i<taskList.size(); i++){
        queueTime=(int)(Math.random()* (240 - 20) + 20);
        task=taskList.get(i);
        
        //hardcode taskQueueTime
        if (queueTime<task.getTaskDeadline()){
            task.setTaskInQueue(queueTime);
        }
        else {
            task.setTaskInQueue(task.getTaskDeadline()-5);
        }
        
        //assign tasks to ICUs with matching skills
        for (int j=0; j<icus.size();j++){
            totaltasks=0;
            icu=icus.get(j);
  
            
            if(task.getSkillReq().equals(icu.getICUSkill())){
            icu.totaltasks_++;
            will=icu.getWillingness();
           
            icu.getICUMetrics().updateICUTaskQueue(task);
            icu.ICUMetrics_.setTotalAssignedTasks(icu.getICUTaskQueue().size());
            task.setTaskOwner(icu);
              //icu.ICUTasks.add(task);
                if((!currentSCU.contains(icu))){
                    currentSCU.addICU(icu, currentSCU); //add(icu);
                }
            task.setTaskState(6);
            
            System.out.println("Task with id "+task.getTaskId()+"is assigned to ICU with id"+icu.getICUId());
            break;
           }
        }   
    }
    
    //check which ICUs with which Ids belong to the SCU currently
    currentSCUMembers();
    int a;
    //simple FCFS delegations
    for (min=0; min<300; min++){ 
        
            for(int j=0; j<taskList.size(); j++){
                tempTask=taskList.get(j);
                //tempTask.taskDelegated=false;
                tempTime=tempTask.getTaskInQueue();
                if(tempTime==min && (tempTime == tempTask.getTaskDeadline()/2)){
                //if(tempTime==min && (tempTime ==(currentICU.getICUTaskQueue().get(j).getTaskDeadline()/2))){
                    currentICU=tempTask.getTaskOwner();
                    System.out.println();
                    System.out.println("Task waiting-time for task with id "+tempTask.getTaskId()+" reached the pre-set threshold, delegating task...");
               
                    
                    for (a=0; a<icus.size(); a++){
                        // totaldelt=0; totalnondelt=0; sucdelt=0; sucnondelt=0;
                        
                        icutemp=icus.get(a);  
                       
                        if(tempTask.getTaskOwner().getICUId()!=icutemp.getICUId()){
                        if(tempTask.getSkillReq().equals(icutemp.getICUSkill())){
                          
                            icutemp.updateICUTaskQueue(tempTask);
                            icutemp.ICUMetrics_.updateICUTaskQueue(tempTask);
                            currentICU.ICUMetrics_.removeTask(tempTask);
                            
                            icutemp.ICUMetrics_.updateDelegatedTasks(tempTask);
                            
                             if((!currentSCU.contains(icutemp))){
                                currentSCU.addICU(icutemp, currentSCU); //add(icu);
                            }
                             
                           
                        System.out.println("Task "+tempTask.getTaskId()+" delegated from ICU with id "+ tempTask.getTaskOwner().getICUId() + " to ICU with id "+ icutemp.getICUId());
                        tempTask.setTaskState(9);
                       // icutemp.ICUMetrics_.
                        tempTask.setTaskOwner(icutemp);
                        icutemp.ICUMetrics_.getAllICUTasks().getTaskwithId(tempTask.getTaskId()).taskDelegated=true;
                                              
                        break;
                        }
                        }
                    //break
                   } 
                }
            }
    }
   String indent = "    ";
   System.out.println();
   System.out.println("========== OUTPUT ==========");
   System.out.println("ICU ID" + indent + "alltasks" + indent + "mcts" + indent + "ptrust" + indent  + "stt"+indent+"succ tasks"); 
   
    for (int k=0; k<icus.size();k++){
    tempICU=icus.get(k);
 
    tempICU.ICUMetrics_.setMICUid(tempICU.getICUId());
    tempICU.ICUMetrics_.calICUCollaborationScore();
    mcts=tempICU.ICUMetrics_.getMCTS();

    tempICU.ICUMetrics_.setMCTS(mcts);
      
    tempICU.ICUMetrics_.calNonDelTasks();
    
    tempICU.ICUMetrics_.calSuccessfulDelNonDelTasks(tempICU.ICUMetrics_.getAllICUTasks());
    
    sucdelt=tempICU.ICUMetrics_.getSuccDelTasks();
    
    sucnondelt=tempICU.ICUMetrics_.getSuccNonDelTasks();
 
    totaldelt=tempICU.ICUMetrics_.getTotalDelTasks();
 
    totaltemp= tempICU.ICUMetrics_.getAllICUTasks().size();
    totalnondelt=totaltemp-totaldelt;

    icutemp.ICUMetrics_.setTotalNonDelTasks(totalnondelt);
    icutemp.ICUMetrics_.setSuccNonDelTasks(sucnondelt);

    
    tempICU.ICUMetrics_.calDelNonDelSuccessRate(tempICU.ICUMetrics_.getAllICUTasks());
    
    
    delsucr=tempICU.ICUMetrics_.getDelegatedSuccessRate();
    nondelsucr=tempICU.ICUMetrics_.getNonDelSuccessRate();
    
       
    
    tempICU.ICUMetrics_.calSuccessRate(icus.get(k).ICUMetrics_.getAllICUTasks());
    sucr=tempICU.ICUMetrics_.getICUSuccessRate();
    
    
    tempICU.ICUMetrics_.calICUreliability();
    reliab=tempICU.ICUMetrics_.getICUreliability();
    
    tempICU.ICUMetrics_.calICUProductivity(icus.get(k).ICUMetrics_.getAllICUTasks());
    prod=tempICU.ICUMetrics_.getICUProductivity();
    
    
    tempICU.ICUMetrics_.calICUwillingnessConf(icus.get(k));
    willconf=tempICU.ICUMetrics_.getICUWillingnessConf();
    
    
    tempICU.ICUMetrics_.calICUPerformanceTrustScore();
    
    pt=tempICU.ICUMetrics_.getICUPerformanceTrustScore();
    tempICU.ICUMetrics_.calICUsocioTechnicalTrust();
    
    socioTechnicalTrust=tempICU.ICUMetrics_.getICUSocioTechnicalTrust();
    
    tempICU.ICUMetrics_.setTotalAssignedTasks(totaltemp);
    tempICU.ICUMetrics_.setSuccDelTasks(sucdelt);
    tempICU.ICUMetrics_.setTotalDelTasks(totaldelt);
    tempICU.ICUMetrics_.setSuccessRate(sucr);
    tempICU.ICUMetrics_.setDelegatedSuccessRate(delsucr);
    tempICU.ICUMetrics_.setNonDelSuccessRate(nondelsucr);
    tempICU.ICUMetrics_.setICUReliability(reliab);
    tempICU.ICUMetrics_.setWillingnessConf(willconf);
    tempICU.ICUMetrics_.setICUPerformanceTrustScore(pt);
    tempICU.ICUMetrics_.setICUProductivity(prod);
    tempICU.ICUMetrics_.setSocioTechnicalTrust(socioTechnicalTrust);
    
     
    metricsList.add(tempICU.ICUMetrics_);
    setMetricList(metricsList);
        System.out.println(icus.get(k).getICUId()+indent+indent+icus.get(k).ICUMetrics_.getAllICUTasks().size()+indent+indent+String.format("%.4f",tempICU.ICUMetrics_.getMCTS())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUPerformanceTrustScore())+indent+String.format("%.4f",tempICU.ICUMetrics_.getICUSocioTechnicalTrust())+indent+indent+icus.get(k).ICUMetrics_.getSuccessfulICUTaskQueue().size());    
  
  
    }
    
    return icus;
}


public static void main(String[] args) throws JAXBException, IOException, FileNotFoundException, URISyntaxException {
STTAlgorithm_ALLICUS_oneSCUInvocation sttalgo = new STTAlgorithm_ALLICUS_oneSCUInvocation();
   tempicus=sttalgo.algo();
   //marshallICUs();
}


public static Map<ICU, Metrics> getMap(){
    return myMap;
}


public void setMetricList(MetricsList metricsList){
    this.metricsList=metricsList;
}


public MetricsList getMetricList(){
    return metricsList;
}



public static void currentSCUMembers(){
    currentSCU.setSCUMembers(currentSCU);
    System.out.print("ICUs in the current SCU are those with Ids: ");
    for(int j=0;j<currentSCU.getSCUMembers().size();j++){
        System.out.print(currentSCU.get(j).getICUId()+" ");
        
    
    }//*/
                   
}  
}
