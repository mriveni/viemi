/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.dsg.infosys.process.engine;

import java.util.ArrayList;
import java.util.List;
import dsg.infosys.common.entity.runtime.DataPartitionRequest;
import dsg.infosys.common.utils.JAXBUtils;
import dsg.infosys.common.utils.RestfulWSClient;
import javax.xml.bind.JAXBException;
import dsg.infosys.common.entity.eda.elasticprocess.Action;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.eda.elasticprocess.DirectedAcyclicalGraph;
import dsg.infosys.common.entity.eda.elasticprocess.ParallelGateway;
import dsg.infosys.common.entity.primitiveaction.AdjustmentAction;
import dsg.infosys.common.entity.runtime.ExecutionStep;
import dsg.infosys.process.engine.dataelasticitycontroller.DEPExecutionPlanning;

/**
 *
 * @author Jun
 */
public class TestSample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here


        SampleProcess sampleProcess = new SampleProcess();
        SLAChangeProcess adjustmentProcess = sampleProcess.getSampleProcess();

        DEPExecutionPlanning depep = new DEPExecutionPlanning(adjustmentProcess);
        List<ExecutionStep> executionPlan = depep.planningExecution();
        


//
//        for (ExecutionStep executionStep : executionPlan) {
//            List<String> listOfActionsInStep = executionStep.getListOfExecutionActions();
//            for (String actionId : listOfActionsInStep) {
//                System.out.println(actionId);
//            }
//        }
    }

}
