/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.dsg.infosys.process.engine;

import java.util.ArrayList;
import java.util.List;
import dsg.infosys.common.entity.eda.elasticprocess.Action;
import dsg.infosys.common.entity.eda.elasticprocess.SLAChangeProcess;
import dsg.infosys.common.entity.eda.elasticprocess.DirectedAcyclicalGraph;
import dsg.infosys.common.entity.eda.elasticprocess.ParallelGateway;

public class SampleProcess {

    public SLAChangeProcess getSampleProcess() {
        List<Action> listOfActions = new ArrayList<>();

        {
            String actionId = "t1";
            String actionName = "task1";
            String incomming = null;
            String outgoing = "g1";

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
        }

        {
            String actionId = "t2";
            String actionName = "task2";
            String incomming = "g1";
            String outgoing = "g2";

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
        }

        {
            String actionId = "t3";
            String actionName = "task3";
            String incomming = "g1";
            String outgoing = "g2";

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
        }

        {
            String actionId = "t4";
            String actionName = "task4";
            String incomming = "g2";
            String outgoing = null;

            Action action = new Action(actionId, actionName);
            action.setIncomming(incomming);
            action.setOutgoing(outgoing);
            listOfActions.add(action);
        }

        List<ParallelGateway> listOfParallelGateways = new ArrayList<>();

        {

            String gatewayId = "g1";
            
            List<String> listOfIncommings = new ArrayList<String>() {
                {
                    add("t1");
                }
            };
            
            List<String> listOfOutgoings = new ArrayList<String>(){
                {
                    add("t2");
                    add("t3");
                }
            };
            
            
            ParallelGateway parallelGateway = new ParallelGateway(gatewayId, listOfIncommings, listOfOutgoings);
            listOfParallelGateways.add(parallelGateway);
        }
        
        {

            String gatewayId = "g2";
            
            List<String> listOfIncommings = new ArrayList<String>() {
                {
                    add("t2");
                    add("t3");
                }
            };
            
            List<String> listOfOutgoings = new ArrayList<String>(){
                {                   
                    add("t4");
                }
            };
            
            
            ParallelGateway parallelGateway = new ParallelGateway(gatewayId, listOfIncommings, listOfOutgoings);
            listOfParallelGateways.add(parallelGateway);
        }
        
        
        

        DirectedAcyclicalGraph dag = new DirectedAcyclicalGraph();
        dag.setListOfActions(listOfActions);
        dag.setListOfParallelGateways(listOfParallelGateways);

        SLAChangeProcess adjustmentProcess = new SLAChangeProcess( null, dag);

        return adjustmentProcess;
    }
}
